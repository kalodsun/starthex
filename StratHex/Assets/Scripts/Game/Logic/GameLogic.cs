﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.TiledMapPathfinder;
using UnityEngine;
using UnityEngine.Assertions;

using Random = UnityEngine.Random;

// ReSharper disable UseNameofExpression

namespace Assets.Scripts.Game.Logic
{
    public class UnitAction
    {
        public enum ActionType
        {
            Move, Bombard, Retreat, Fight, Attack
        }

        public ActionType Type { get; set; }
        public HexCell Target { get; set; }
        public float Progress { get; set; }

        public UnitAction(ActionType type, HexCell target)
        {
            Type = type;
            Target = target;
            Progress = 0f;
        }
    }

    public class UnitLogicData
    {
        public Dictionary<Order, HexMapPath> paths = new Dictionary<Order, HexMapPath>();
        public List<Order> orders = new List<Order>();
        public UnitAction currentAction;

        public HashSet<Battle> currentBattles = new HashSet<Battle>();
    }

    public class SavedGameState
    {
        public Dictionary<Unit, UnitLogicData> unitLogicData;
        public List<Unit> Units;
    }

    public class GameLogic
    {
        public static GameLogic Instance { get; private set; }

        private int turnNumber = 0;

        public GameData gameData;
        public HexGrid hexGrid;

        public List<Structure> Structures { get; private set; }
        public List<Unit> Units { get; private set; }

        public Dictionary<Unit, UnitLogicData> unitLogicData = new Dictionary<Unit, UnitLogicData>();
        public readonly Dictionary<HexCell, Unit> unitsByTile = new Dictionary<HexCell, Unit>();

        public readonly Dictionary<HexCell, Battle> activeBattles = new Dictionary<HexCell, Battle>();

        private readonly Dictionary<TerrainType, float> terrainMovementCost = new Dictionary<TerrainType, float>();
        private float forestMovementCost, urbanMovmentCost, ruralMovmentCost;

        private Dictionary<String, EdgeCostDelegate> unitMoveDelegates = new Dictionary<string, EdgeCostDelegate>();

        private void ClearAllData()
        {
            Units.Clear();
            unitLogicData.Clear();

            unitsByTile.Clear();

            activeBattles.Clear();
        }

        private void InitMovementCosts()
        {
            terrainMovementCost.Add(TerrainType.Grass, 0);
            terrainMovementCost.Add(TerrainType.DryGrass, 0);
            terrainMovementCost.Add(TerrainType.Snow, 1);
            terrainMovementCost.Add(TerrainType.Rock, 2);
            terrainMovementCost.Add(TerrainType.Sand, 1);
            terrainMovementCost.Add(TerrainType.Urban, 1);

            forestMovementCost = 0.5f;
            urbanMovmentCost = 0.35f;
            ruralMovmentCost = 0f;

            foreach (KeyValuePair<string, Unit.StatsCls> keyValuePair in gameData.UnitTypes)
            {
                KeyValuePair<string, Unit.StatsCls> pair = keyValuePair;
                unitMoveDelegates.Add(keyValuePair.Value.Name, delegate(HexCell from, HexCell to)
                {
                    return GetMovementCost(pair.Value, from, to);
                });
            }
        }

        public GameLogic(GameData gameData, HexGrid hexGrid)
        {
            this.gameData = gameData;
            this.hexGrid = hexGrid;

            Assert.IsNull(Instance);
            Instance = this;

            Units = new List<Unit>();
            Structures = new List<Structure>();

            InitMovementCosts();
        }

        public void LoadGameState(SavedGameState gameState)
        {
            ClearAllData();

            if (gameState != null) {
                Units = gameState.Units;
                unitLogicData = gameState.unitLogicData;

                foreach (Unit unit in Units) {
                    unitsByTile[unit.CurrentCell] = unit;
                }
            }

            GameEventSystem.SendGameEvent(GameEventSystem.GameEventType.GameReloaded);
        }

        public SavedGameState CreateSaveGameState()
        {
            SavedGameState ret = new SavedGameState();

            ret.Units = new List<Unit>(Units);
            ret.unitLogicData = new Dictionary<Unit, UnitLogicData>(unitLogicData);

            return ret;
        }

        public int TurnNumber
        {
            get { return turnNumber; }
        }

        public Unit GetUnitAtCell(HexCell cell)
        {
            return unitsByTile.ContainsKey(cell) ? unitsByTile[cell] : null;
        }

        public Unit CreateUnit(Unit.StatsCls stats, HexCell cell, HexDirection facing, Faction faction, bool force = false)
        {
            if (!force && GetUnitAtCell(cell) != null)
                return null;

            Unit newUnit = new Unit(stats);
            newUnit.Faction = faction;
            newUnit.SetCurrentCell(cell);
            newUnit.Facing = facing;
            Units.Add(newUnit);

            unitLogicData.Add(newUnit, new UnitLogicData());

            unitsByTile[cell] = newUnit;

            return newUnit;
        }

        public void DestroyUnit(Unit unit)
        {
            if (!unitLogicData.ContainsKey(unit))
            {
                // unit is already destroyed
                Debug.LogWarning("Attempted to destroy a unit that is already removed: " + unit.Stats.VisibleName);
                return;
            }

            //unit.SetCurrentCell(null); Is it necessary?
            unit.Properties.Values[Unit.StatValues.Strength] = -1;

            unitLogicData.Remove(unit);
            RemoveUnitFromAllBattles(unit);
            unitsByTile[unit.CurrentCell] = null;
        }

        protected void MoveUnit(Unit unit, HexCell cell)
        {
            unitsByTile[unit.CurrentCell] = null;

            unit.SetCurrentCell(cell);

            unitsByTile[unit.CurrentCell] = unit;
        }

        public Structure CreateStructure(Structure.StatsCls stats, HexCell cell, HexDirection facing = 0)
        {
            Structure newStructure = new Structure(stats, cell);
            newStructure.Direction = facing;
            Structures.Add(newStructure);

            return newStructure;
        }

        public void QueueOrder(Unit unit, Order order)
        {
            if (!unitLogicData.ContainsKey(unit))
                throw new Exception("Attempted to issue order for invalid unit!");

            unitLogicData[unit].orders.Add(order);

            UpdateUnitPaths(unit);
        }

        public void ClearOrders(Unit unit)
        {
            if (!unitLogicData.ContainsKey(unit))
                throw new Exception("Attempted to clear orders for invalid unit!");

            unitLogicData[unit].orders.Clear();
        }

        public void ProcessTurn()
        {
            UpdateAllPaths();

            ApplyTurnStartEffects();

            UpdateActions();

            //queuedActions.Sort(new UnitAction.Comparer());
            //TODO action ordering

            ExecuteActions();

            ProcessBattles();

            UpdateOrders();

            turnNumber++;

            // Update unit logic data for the benefit of the UI
            UpdateActions();
            UpdateAllPaths();

            GameEventSystem.SendGameEvent(GameEventSystem.GameEventType.TurnProcessed);
        }

        public void ApplyTurnStartEffects()
        {
            foreach (Unit unit in Units) {
                if (unit.Alive) {
                    float maxMorale = unit.Stats.Values[Unit.StatValues.Morale];
                    float morale = unit.Properties.Values[Unit.StatValues.Morale];

                    float moraleRegenMult = unitLogicData[unit].currentBattles.Count > 0 ? 0.01f : 0.05f;

                    unit.Properties.Values[Unit.StatValues.Morale] = Mathf.Min(morale + moraleRegenMult * maxMorale, maxMorale);

                    morale = unit.Properties.Values[Unit.StatValues.Morale];
                    float moralePercentage = morale / maxMorale;

                    float maxOrg = unit.Stats.Values[Unit.StatValues.Organization];
                    float org = unit.Properties.Values[Unit.StatValues.Organization];

                    float orgRegenMult = unitLogicData[unit].currentBattles.Count > 0 ? 0.02f : 0.05f;

                    unit.Properties.Values[Unit.StatValues.Organization] = Mathf.Min(org + orgRegenMult * maxOrg * moralePercentage, maxOrg);
                }
            }
        }

        public void UpdateUnitPaths(Unit unit)
        {
            unitLogicData[unit].paths.Clear();

            HexCell orderStartCell = unit.CurrentCell;
            foreach (var order in unitLogicData[unit].orders)
            {
                if (order.Type == OrderType.Move)
                {
                    unitLogicData[unit].paths.Add(order, hexGrid.pathfinder.FindPath(orderStartCell, order.Target, GetOptimalMovementDelegate(unit.Stats)));

                    if (unitLogicData[unit].paths[order] == null)
                        break;

                    orderStartCell = order.Target;
                }
            }
        }

        public void UpdateAllPaths()
        {
            foreach (Unit unit in Units)
            {
                if (unit.Alive)
                {
                    UpdateUnitPaths(unit);
                }
            }
        }

        private void UpdateActions()
        {
            foreach (Unit unit in Units)
            {
                if (!unit.Alive) {
                    continue;
                }

                if (unitLogicData[unit].orders.Count > 0) {
                    Order curOrder = unitLogicData[unit].orders[0];

                    UnitAction createdAction = null;
                    switch (curOrder.Type) {
                        case OrderType.Move:
                            UpdateUnitPaths(unit);
                            HexMapPath path = unitLogicData[unit].paths[curOrder];

                            if (path == null) {
                                ClearOrders(unit);
                                unitLogicData[unit].currentAction = null;
                            } else {
                                
                                if (unitLogicData[unit].currentBattles.Count == 0) {
                                    createdAction = new UnitAction(UnitAction.ActionType.Move, path.PathHexes[1]);
                                } else {
                                    createdAction = new UnitAction(UnitAction.ActionType.Retreat, path.PathHexes[1]); ;
                                }
                            }
                            break;
                        case OrderType.Bombard:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    // if we are already doing the correct thing, don't change the action
                    UnitAction currentAction = unitLogicData[unit].currentAction;

                    bool isNewActionDifferent = createdAction != null && (currentAction == null || 
                                          ((createdAction.Type != currentAction.Type ||
                                           createdAction.Target != currentAction.Target)));

                    if (isNewActionDifferent) {
                        if (createdAction.Type == UnitAction.ActionType.Retreat && currentAction != null && currentAction.Type == UnitAction.ActionType.Fight) {
                            unitLogicData[unit].currentAction = createdAction;
                        } else if (currentAction == null ||
                            (currentAction.Type != UnitAction.ActionType.Fight &&
                            currentAction.Type != UnitAction.ActionType.Retreat)) {
                            unitLogicData[unit].currentAction = createdAction;
                        }
                    }
                }
            }
        }

        private bool IsActionPossible(UnitAction action, Unit unit)
        {
            switch (action.Type) {
                case UnitAction.ActionType.Move:
                    return GetMovementCost(unit.Stats, unit.CurrentCell, action.Target) > 0;
                case UnitAction.ActionType.Attack:
                    return GetMovementCost(unit.Stats, unit.CurrentCell, action.Target) > 0 && CombatUtils.CanAttack(unit);
                case UnitAction.ActionType.Retreat:
                    return GetMovementCost(unit.Stats, unit.CurrentCell, action.Target) > 0 && GetUnitAtCell(action.Target) == null;
                default:
                    return false;
            }
        }

        enum ActionResult { InProgress, Executed, Completed, Failed, Blocked }

        private void ExecuteActions()
        {
            foreach (Unit unit in Units) {
                if (!unit.Alive) {
                    continue;
                }

                UnitAction curAction = unitLogicData[unit].currentAction;

                if (curAction == null) {
                    continue;
                }

                ActionResult result = ActionResult.InProgress;

                Unit targetCellUnit = GetUnitAtCell(curAction.Target);
                switch (curAction.Type)
                {
                    case UnitAction.ActionType.Move:
                        if (targetCellUnit == null) {
                            result = ExecuteMoveAction(curAction, unit);
                        } else {
                            if (unit.Faction.GetRelation(targetCellUnit.Faction) == Faction.Relation.Enemy) {
                                result = ExecuteAttack(curAction, unit);
                            } else {
                                result = ActionResult.Blocked;
                            }
                        }

                        if (result == ActionResult.Failed) {
                            ClearOrders(unit);
                        }
                        break;
                    case UnitAction.ActionType.Bombard:
                        break;
                    case UnitAction.ActionType.Retreat:
                        result = ExecuteRetreatAction(curAction, unit);

                        if (result == ActionResult.Failed) {
                            SurrenderUnit(unit);
                        }
                        break;
                    case UnitAction.ActionType.Fight:
                        // nothing to do here, processing is done in ProcessBattles()
                        result = ActionResult.Executed;
                        break;
                    default:
                        Debug.LogError("Action type not implemented: " + curAction.Type);
                        break;
                        //throw new ArgumentOutOfRangeException();
                }

                if (result == ActionResult.Completed) {
                    if (unitLogicData.ContainsKey(unit)) {
                        unitLogicData[unit].currentAction = null;
                    }
                }
            }
        }

        public EdgeCostDelegate DirectMovementDelegate = delegate (HexCell from, HexCell to)
        {
            if (to.HasRiver)
                return -1;

            return 1;
        };

        public EdgeCostDelegate GetOptimalMovementDelegate(Unit.StatsCls unitType)
        {
            return unitMoveDelegates[unitType.Name];
        }

        private void UpdateOrders()
        {
            foreach (Unit unit in Units)
            {
                if(!unit.Alive)
                    continue;

                while (unitLogicData[unit].orders.Count > 0)
                {
                    bool orderComplete = false;

                    switch (unitLogicData[unit].orders[0].Type)
                    {
                        case OrderType.Move:
                            if (unit.CurrentCell == unitLogicData[unit].orders[0].Target)
                                orderComplete = true;
                            break;
                        case OrderType.Bombard:
                            orderComplete = true;
                            break;
                    }

                    if(orderComplete)
                        unitLogicData[unit].orders.RemoveAt(0);
                    else
                        break;
                }
            }
        }

        private ActionResult ExecuteRetreatAction(UnitAction action, Unit unit)
        {
            if (!IsActionPossible(action, unit)) {
                return ActionResult.Failed;
            }

            ActionResult result = ExecuteMoveAction(action, unit);

            if (result == ActionResult.Completed) {
                RemoveUnitFromAllBattles(unit);
            }

            return result;
        }

        private ActionResult ExecuteMoveAction(UnitAction action, Unit unit)
        {
            if (!IsActionPossible(action, unit)) {
                return ActionResult.Failed;
            }

            unit.Facing = HexUtils.GetDirection(unit.CurrentCell, action.Target);
            float moveCost = GetMovementCost(unit.Stats, unit.CurrentCell, action.Target);

            if (moveCost < action.Progress) {
                MoveUnit(unit, action.Target);
                return ActionResult.Completed;
            } else {
                action.Progress += unit.Stats.Values[Unit.StatValues.Speed];
                return ActionResult.Executed;
            }
        }

        private ActionResult ExecuteAttack(UnitAction action, Unit unit)
        {
            if (!IsActionPossible(action, unit)) {
                return ActionResult.Failed;
            }

            if (!activeBattles.ContainsKey(action.Target)) {
                BeginBattle(action.Target);
            }

            AddUnitToBattle(activeBattles[action.Target], unit);
            unit.Facing = HexUtils.GetDirection(unit.CurrentCell, action.Target);

            unitLogicData[unit].currentAction = new UnitAction(UnitAction.ActionType.Fight, action.Target);

            return ActionResult.Executed;
        }

        private void BeginBattle(HexCell cell)
        {
            if (activeBattles.ContainsKey(cell)) {
                Debug.LogError("Attempted to start a battle in a cell with one");
                return;
            }

            Battle battle = new Battle(cell);

            Unit defender = unitsByTile[cell];

            battle.Defenders.Add(defender);
            unitLogicData[defender].currentAction = new UnitAction(UnitAction.ActionType.Fight, cell);
            unitLogicData[defender].currentBattles.Add(battle);

            activeBattles.Add(cell, battle);
        }

        private void EndBattle(Battle battle)
        {
            for (int i = battle.Attackers.Count-1; i >= 0; i--) {
                Unit unit = battle.Attackers[i];

                unitLogicData[unit].currentAction = null;
                RemoveUnitFromBattle(battle, unit);
            }

            for (int i = battle.Defenders.Count-1;i >= 0;i--) {
                Unit unit = battle.Defenders[i];

                unitLogicData[unit].currentAction = null;
                RemoveUnitFromBattle(battle, unit);
            }

            activeBattles.Remove(battle.BattleCell);
        }

        private void AddUnitToBattle(Battle battle, Unit unit)
        {
            if (unit.CurrentCell == battle.BattleCell) {
                battle.Defenders.Add(unit);
            } else {
                battle.Attackers.Add(unit);
            }

            unitLogicData[unit].currentBattles.Add(battle);
            ClearOrders(unit);
        }

        private void RemoveUnitFromAllBattles(Unit unit)
        {
            foreach (KeyValuePair<HexCell, Battle> battle in activeBattles) {
                if (battle.Value.Defenders.Contains(unit) || battle.Value.Attackers.Contains(unit)) {
                    RemoveUnitFromBattle(battle.Value, unit);
                }
            }
        }

        private void RemoveUnitFromBattle(Battle battle, Unit unit)
        {
            if (battle.Attackers.Contains(unit)) {
                battle.Attackers.Remove(unit);
            } else {
                battle.Defenders.Remove(unit);
            }

            unitLogicData[unit].currentBattles.Remove(battle);
        }

        private void ProcessBattles()
        {
            List<HexCell> battleSites = activeBattles.Keys.ToList();
            for (int battleIdx = 0; battleIdx < battleSites.Count; battleIdx++) {
                Battle currentBattle = activeBattles[battleSites[battleIdx]];

                // Carry out attacks
                if (CombatUtils.CanBattleProgress(currentBattle, unitLogicData)) {

                    ProcessCombatRound(currentBattle);
                }

                // Update unit states, move retreating and remove killed
                for (int attackerIdx = 0;attackerIdx < currentBattle.Attackers.Count;attackerIdx++) {
                    Unit attacker = currentBattle.Attackers[attackerIdx];

                    UpdateCombatantAction(attacker);
                }

                for (int defenderIdx = 0; defenderIdx < currentBattle.Defenders.Count; defenderIdx++) {
                    Unit defender = currentBattle.Defenders[defenderIdx];

                    UpdateCombatantAction(defender);
                }

                if (!CombatUtils.CanBattleProgress(currentBattle, unitLogicData)) {
                    EndBattle(currentBattle);
                }
            }
        }

        private void UpdateCombatantAction(Unit combatant)
        {
            UnitLogicData logicData = unitLogicData[combatant];

            UnitAction.ActionType actionType = logicData.currentAction.Type;

            float orgThreshold = 30;
            float moraleThreshold = 30;

            if (actionType == UnitAction.ActionType.Fight &&
                (combatant.Properties.Values[Unit.StatValues.Organization] < orgThreshold ||
                 combatant.Properties.Values[Unit.StatValues.Morale] < moraleThreshold)) {

                BeginUnitRetreat(combatant);
            }

            if (combatant.Properties.Values[Unit.StatValues.Morale] < 0) {
                SurrenderUnit(combatant);
            }
        }

        public void ProcessCombatRound(Battle battle)
        {
            foreach (var attacker in battle.Attackers) {

                var defender = battle.Defenders[Random.Range(0, battle.Defenders.Count)];

                CombatUtils.ProcessSingleAttackRound(battle, unitLogicData, defender, attacker);
            }
        }

        // Picks the best retreat direction based on surrounding units and terrain.
        // Note that it might still not be a valid way to retreat, its just the best available.
        private HexDirection GetRetreatDirection(Unit unit)
        {
            Debug.Assert(activeBattles.ContainsKey(unit.CurrentCell));

            Battle battle = activeBattles[unit.CurrentCell];

            int[] dirPenalty = new int[6];
            foreach (Unit attacker in battle.Attackers) {
                HexDirection dir = HexUtils.GetDirection(unit.CurrentCell, attacker.CurrentCell);

                dirPenalty[(int)dir] += 100;

                dirPenalty[(int)dir.Previous()] += 50;
                dirPenalty[(int)dir.Next()] += 50;

                dirPenalty[(int)dir.Previous().Previous()] += 1;
                dirPenalty[(int)dir.Next().Next()] += 1;
            }

            int minPenalty = Int32.MaxValue;
            int minIdx = 0;
            for (int i = 0; i < 6; i++) {
                float cost = GetMovementCost(unit.Stats, unit.CurrentCell, unit.CurrentCell.GetNeighbor((HexDirection) i));

                if (cost < 0) {
                    dirPenalty[i] += 1000;
                } else {
                    dirPenalty[i] += Mathf.RoundToInt(cost);
                }

                if (minPenalty > dirPenalty[i]) {
                    minPenalty = dirPenalty[i];
                    minIdx = i;
                }
            }

            return (HexDirection) minIdx;
        }

        private void BeginUnitRetreat(Unit unit)
        {
            if (!activeBattles.ContainsKey(unit.CurrentCell)) {
                RemoveUnitFromAllBattles(unit);

                unitLogicData[unit].currentAction = null;
            } else {
                HexDirection direction = GetRetreatDirection(unit);

                HexCell retreatCell = unit.CurrentCell.GetNeighbor(direction);

                unitLogicData[unit].currentAction = new UnitAction(UnitAction.ActionType.Retreat, retreatCell);
            }
        }

        private void SurrenderUnit(Unit unit)
        {
            RemoveUnitFromAllBattles(unit);

            DestroyUnit(unit);
        }

        private float GetMovementCost(Unit.StatsCls unitType, HexCell startCell, HexCell endCell)
        {
            if (endCell.IsUnderwater)
                return -1;

            float destinationCost, borderCost, unitCapabilty;

            unitCapabilty = unitType.Values[Unit.StatValues.MovementCapabilty];

            destinationCost = terrainMovementCost[endCell.TerrainType];
            destinationCost += endCell.ForestLevel * forestMovementCost + endCell.UrbanLevel * urbanMovmentCost + endCell.RuralLevel * ruralMovmentCost;

            borderCost = Mathf.Abs(startCell.Elevation - endCell.Elevation);

            bool hasRoad = endCell.HasRoadThroughEdge(HexUtils.GetDirection(endCell, startCell));

            if (hasRoad)
                destinationCost = borderCost = 0;
            else if (endCell.HasRiver)
                return -1;

            if (destinationCost > unitCapabilty * 2f)
                return -1;

            return 1f + Mathf.Max(destinationCost - unitCapabilty, 0f) + Mathf.Pow(borderCost, 1.5f);
        }
    }
}
