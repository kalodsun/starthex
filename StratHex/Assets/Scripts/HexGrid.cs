﻿using System;
using System.IO;
using Assets.Scripts.Game.TiledMapPathfinder;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class HexGrid : MonoBehaviour {

        public int chunkCountX = 6, chunkCountZ = 6;
        public int chunkSizeX = 5, chunkSizeZ = 5;
        private int cellCountX, cellCountZ;
        public float cellSize = 10f;
        public float elevationStep = 5f;
        public int seed;

        public int SizeX
        {
            get { return cellCountX; }
        }

        public int SizeZ
        {
            get { return cellCountZ; }
        }

        public HexGridChunk chunkPrefab;
        public HexCell cellPrefab;
        public Text cellTextPrefab;
        public Texture2D noiseSource, Heightmap;

        HexCell[] cells;
        HexGridChunk[] chunks;

        public IMapPathfinder pathfinder;

        public static HexGrid Instance { get; private set; }

        private readonly EdgeCostDelegate defaultEdgeWeight = delegate(HexCell from, HexCell to)
        {
            if (to.HasRiver)
                return -1;

            float heightMul = Mathf.Abs(from.Elevation - to.Elevation) + 1;

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                if (from.GetNeighbor(d) == to)
                {
                    if (from.HasRoadThroughEdge(d))
                        return 1 * heightMul;
                    return 6 * heightMul;
                }
            }

            return -1;
        };

        void Awake()
        {
            Instance = this;

            HexUtils.noiseSource = noiseSource;
            HexUtils.InitializeHashGrid(seed);

            cellCountX = chunkCountX * chunkSizeX;
            cellCountZ = chunkCountZ * chunkSizeZ;

            cells = new HexCell[cellCountZ * cellCountX];

            CreateChunks();
            CreateCells();

            ApplyHeightmap(Heightmap, new RectInt(0, 0, Math.Min(cellCountX, Heightmap.width), Math.Min(cellCountZ, Heightmap.height)));

            RefreshAllCells();
            pathfinder = new AStarPathfinder(this, defaultEdgeWeight);
        }

        public void Recreate(int newSizeX, int newSizeZ)
        {
            cellCountX = newSizeX;
            cellCountZ = newSizeZ;

            chunkCountX = newSizeX/chunkSizeX + (newSizeX%chunkSizeX == 0 ? 0 : 1);
            chunkCountZ = newSizeZ/chunkSizeZ + (newSizeZ%chunkSizeZ == 0 ? 0 : 1);

            cells = new HexCell[cellCountZ * cellCountX];

            CreateChunks();
            CreateCells();
        }

        public void RebuildPathfinding()
        {
            pathfinder = new AStarPathfinder(this, defaultEdgeWeight);
        }

        public void RefreshAllCells()
        {
            foreach (HexGridChunk hexGridChunk in chunks)
            {
                hexGridChunk.Refresh();
            }
        }

        void CreateChunks()
        {
            if (chunks != null)
            {
                for (int i = 0; i < chunks.Length; i++)
                {
                    Destroy(chunks[i].gameObject);
                }
            }

            chunks = new HexGridChunk[chunkCountX * chunkCountZ];

            for (int z = 0, i = 0; z < chunkCountZ; z++)
            {
                for (int x = 0; x < chunkCountX; x++)
                {
                    HexGridChunk chunk = chunks[i++] = Instantiate(chunkPrefab);
                    chunk.transform.SetParent(transform);
                }
            }
        }

        void CreateCells()
        {
            for (int z = 0, i = 0; z < cellCountZ; z++)
            {
                for (int x = 0; x < cellCountX; x++)
                {
                    CreateCell(x, z, i++);
                }
            }
        }

        public void ApplyHeightmap(Texture2D heightmap, RectInt activeHeightmapRect, int maxHeight = 20)
        {
            float minGray = float.MaxValue, maxGray = float.MinValue;
            for (int y = 0; y < activeHeightmapRect.height; y++)
                for (int x = 0; x < activeHeightmapRect.width; x++)
                {
                    float pix = heightmap.GetPixel(activeHeightmapRect.x + x, activeHeightmapRect.y + y).grayscale;

                    if (minGray > pix)
                        minGray = pix;

                    if (maxGray < pix)
                        maxGray = pix;
                }

            for (int y = 0; y < cellCountZ; y++)
                for (int x = 0; x < cellCountX; x++) {
                    int hmapX, hmapY;

                    hmapX = activeHeightmapRect.x + activeHeightmapRect.width * x / cellCountX;
                    hmapY = activeHeightmapRect.y + activeHeightmapRect.height * y / cellCountZ;

                    float pix = heightmap.GetPixel(hmapX, hmapY).grayscale;
                    int h = (int) ((pix - minGray)/(maxGray - minGray)* maxHeight);

                    cells[x + y*cellCountX].Elevation = h;
                    cells[x + y*cellCountX].TerrainType = GetTerrain(h, maxHeight);
                }
        }

        public static TerrainType GetTerrain(int height, int maxHeight)
        {
            float f = ((float) height)/maxHeight;

            if (f > 0.8f)
                return TerrainType.Snow;
            if (f > 0.6f)
                return TerrainType.Rock;
            if (f > 0.35f)
                return TerrainType.DryGrass;
            if (f > 0.05f)
                return TerrainType.Grass;
            return TerrainType.Sand;
        }

        void OnEnable()
        {
            if (!HexUtils.noiseSource)
            {
                HexUtils.noiseSource = noiseSource;
                HexUtils.InitializeHashGrid(seed);
            }
        }

        void CreateCell(int x, int z, int i)
        {
            HexCell cell = cells[i] = Instantiate<HexCell>(cellPrefab);
            cell.Grid = this;

            cell.RawCoordinates = new IntVector2(x, z);

            cell.coordinates = HexCoordinates.FromOffsetCoordinates(x, z);
            cell.TerrainType = TerrainType.Sand;

            Vector3 position = cell.coordinates.GetWorldCoordinates(cellSize);
            cell.transform.localPosition = position;

            if (x > 0)
            {
                cell.SetNeighbor(HexDirection.W , cells[i - 1]);
            }

            if (z > 0)
            {
                if ((z & 1) == 0) {
                    cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX]);
                    if (x > 0) {
                        cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX - 1]);
                    }
                } else {
                    cell.SetNeighbor(HexDirection.SW, cells[i - cellCountX]);
                    if (x < cellCountX - 1) {
                        cell.SetNeighbor(HexDirection.SE, cells[i - cellCountX + 1]);
                    }
                }
            }

            Text label = Instantiate<Text>(cellTextPrefab);
            //label.rectTransform.SetParent(gridCanvas.transform, false);
            label.rectTransform.anchoredPosition = new Vector2(position.x, position.z);
            //label.text = cell.RawCoordinates.ToString();//cell.coordinates.ToStringOnSeparateLines();
            cell.uiRect = label.rectTransform;

            AddCellToChunk(x, z, cell);
        }

        void AddCellToChunk(int x, int z, HexCell cell)
        {
            int chunkX = x / chunkSizeX;
            int chunkZ = z / chunkSizeZ;
            HexGridChunk chunk = chunks[chunkX + chunkZ * chunkCountX];

            int localX = x - chunkX * chunkSizeX;
            int localZ = z - chunkZ * chunkSizeZ;
            chunk.AddCell(localX + localZ * chunkSizeX, cell);
        }

        public HexGridChunk GetChunk(Vector3 position)
        {
            int chunkX = (int)(position.x / (chunkSizeX * cellSize));
            int chunkZ = (int)(position.z / (chunkSizeZ * cellSize));
            return chunks[chunkX + chunkZ * chunkCountX];
        }

        public HexCell GetCell(HexCoordinates coordinates)
        {
            int z = coordinates.Z;
            if (z < 0 || z >= cellCountZ)
            {
                return null;
            }

            int x = coordinates.X + z / 2;
            if (x < 0 || x >= cellCountX)
            {
                return null;
            }

            return cells[x + z * cellCountX]; ;
        }

        public HexCell GetCell(Vector3 position)
        {
            position = transform.InverseTransformPoint(position);
            HexCoordinates coordinates = HexUtils.CoordinatesFromPosition(position, cellSize);
            return GetCell(coordinates);
        }

        public HexCell GetCellRawCoordinates(int x, int z)
        {
            return cells[x + z*cellCountX];
        }

        public HexCell[] GetAllCells()
        {
            return cells;
        }
    }
}
