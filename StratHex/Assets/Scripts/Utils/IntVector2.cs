﻿using System;
using UnityEngine;

// ReSharper disable once CheckNamespace
public class IntVector2
{
    public int x, y;

    public IntVector2()
    {
        x = y = 0;
    }

    public IntVector2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public IntVector2(Vector2 v)
    {
        x = Mathf.RoundToInt(v.x);
        y = Mathf.RoundToInt(v.y);
    }

    public override bool Equals(object obj)
    {
        if (obj == null)
            return false;

        IntVector2 v = obj as IntVector2;
        if ((System.Object)v == null)
            return false;

        return Equals(v);
    }

    public bool Equals(IntVector2 v)
    {
        if (v == null)
            return false;

        return (x == v.x) && (y == v.y);
    }

    public override int GetHashCode()
    {
        return x ^ y;
    }

    public override string ToString()
    {
        return "(" + x.ToString() + "," + y.ToString() + ")";
    }

    public static IntVector2 operator +(IntVector2 v1, IntVector2 v2)
    {
        return new IntVector2(v1.x + v2.x, v1.y + v2.y);
    }

    public static IntVector2 operator -(IntVector2 v1, IntVector2 v2)
    {
        return new IntVector2(v1.x - v2.x, v1.y - v2.y);
    }

    public static bool operator ==(IntVector2 v1, IntVector2 v2)
    {
        if (object.ReferenceEquals(v1, v2))
            return true;

        if(((object)v1 == null) || ((object)v2 == null))
            return false;

        return v1.x == v2.x && v1.y == v2.y;
    }

    public static bool operator !=(IntVector2 v1, IntVector2 v2)
    {
        return !(v1 == v2);
    }

    public static implicit operator Vector2(IntVector2 v)
    {
        return new Vector2(v.x, v.y);
    }
}
