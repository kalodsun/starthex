﻿using System.Collections.Generic;

namespace Assets.Scripts.Game.Entities
{
    public class Unit
    {
        public Unit(StatsCls stats)
        {
            Stats = stats;
            Properties = new PropertiesCls(stats);
            Facing = HexDirection.SE;
        }

        public HexDirection Facing { get; set; }
        private HexCell currentCell;
        public HexCell CurrentCell
        {
            get { return currentCell; }
            private set
            {
                currentCell = value;
            }
        }
        public PropertiesCls Properties { private set; get; }
        public StatsCls Stats { private set; get; }

        public void SetCurrentCell(HexCell tile)
        {
            CurrentCell = tile;
        }

        public bool Alive {
            get { return Properties.Values[StatValues.Strength] > 0; }
        }

        public Faction Faction { get; set; }

        public enum StatValues { Strength, Organization, Morale, Attack, Defence, Speed, BombardmentRange, BombardmentAttack, MovementCapabilty }

        public class PropertiesCls
        {
            public PropertiesCls(StatsCls stats)
            {
                Values[StatValues.Strength] = stats.Values[StatValues.Strength];
                Values[StatValues.Organization] = stats.Values[StatValues.Organization];
                Values[StatValues.Morale] = stats.Values[StatValues.Morale];

                UniqueName = stats.VisibleName;
            }

            public Dictionary<StatValues, float> Values = new Dictionary<StatValues, float>();

            public string UniqueName;
        }

        public class StatsCls
        {
            public string Name = "BasicUnit";

            public string PrefabName = "BasicUnit";
            public string CounterName = "DefaultCounter";
            
            public string VisibleName = "NOVISIBLENAME";

            public Dictionary<StatValues, float> Values = new Dictionary<StatValues, float>();

            public void Inherit(StatsCls original)
            {
                PrefabName = original.PrefabName;
                VisibleName = original.VisibleName;
                CounterName = original.CounterName;

                foreach (KeyValuePair<StatValues, float> value in original.Values)
                {
                    Values[value.Key] = value.Value;
                }
            }
        }
    }
}
