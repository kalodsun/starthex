﻿
namespace Assets.Scripts.Game.TiledMapGenerator
{
    interface IMapGenerator
    {
        void GenerateMap(HexGrid map, int seed);
    }
}
