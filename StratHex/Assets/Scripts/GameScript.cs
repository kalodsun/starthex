﻿using System.Collections.Generic;
using System.IO;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.IO;
using Assets.Scripts.Game.Logic;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameScript : MonoBehaviour
    {
        public HexGrid hexGrid;
        public GameData gameData;
        public GameLogic gameLogic;
        public PrefabManager prefabManager;
        public UIScript.UnitGraphicsManager unitGraphicsManager;

        void Awake()
        {
            Dictionary<string, Structure.StatsCls> StructureTypes = null;
            Dictionary<string, Unit.StatsCls> UnitTypes = null;
            Dictionary<string, Faction> FactionTypes = null;

            try
            {
                StructureTypes = DataLoader.LoadStructures("Data\\structures.txt");
            }
            catch (IOException ex)
            {
                Debug.Log(ex);
            }

            try
            {
                UnitTypes = DataLoader.LoadUnits("Data\\units.txt");
            }
            catch (IOException ex)
            {
                Debug.Log(ex);
            }

            try
            {
                FactionTypes = DataLoader.LoadFactions("Data\\factions.txt");
            }
            catch (IOException ex)
            {
                Debug.Log(ex);
            }

            gameData = new GameData(StructureTypes, UnitTypes, FactionTypes);
            gameLogic = new GameLogic(gameData, hexGrid);
            prefabManager = new PrefabManager();
            unitGraphicsManager = new UIScript.UnitGraphicsManager();
        }
    }
}
