﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Assets;
using Assets.Scripts;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.IO;
using Assets.Scripts.Game.Logic;
using Assets.Scripts.Game.TiledMapGenerator;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Application = UnityEngine.Application;
using Button = UnityEngine.UI.Button;

public class UIScript : MonoBehaviour
{
    private const int MOUSE_HOLD_COOLDOWN = 5;

    public enum UIMode
    {
        Gameplay,
        Editor
    }

    public HexGrid hexGrid;
    public GameScript gameScript;

    public GameObject EditorUiGameObject, GameUiGameObject;
    public GameObject UnitBannerPrefab;
    public Button SaveMapButton, LoadMapButton, CreateMapButton;
    public GameObject CreateMapPanel;

    public GameObject BattleInfoPanel, BattleMarkerPrefab;

    public GameObject cameraBase;

    private UIMode currentUiMode;
    bool isDrag;
    HexDirection dragDirection;
    HexCell previousCell;

    private HexMapEditor mapEditorUi;
    private GameplayUI gameplayUi;
    private int mouseCooldownCounter = 0;

    private Dictionary<Unit, UnitBannerScript> unitBanners = new Dictionary<Unit, UnitBannerScript>();

    public class UnitGraphicsManager
    {
        public static UnitGraphicsManager Instance { get; private set; }

        private Dictionary<Unit, UnitGraphics> UnitGraphics;

        public UnitGraphicsManager()
        {
            Assert.IsNull(Instance);
            Instance = this;

            UnitGraphics = new Dictionary<Unit, UnitGraphics>();
        }

        public void ClearGraphics()
        {
            List<Unit> graphicsToRemove = new List<Unit>();
            foreach (Unit key in UnitGraphics.Keys) {
                if (!key.Alive) {
                    graphicsToRemove.Add(key);
                }
            }

            foreach (Unit key in graphicsToRemove) {
                DestroyGraphics(key);
            }
        }

        public void UpdateGraphics(Unit unit)
        {
            if (UnitGraphics.ContainsKey(unit)) {
                UnitGraphics[unit].RefreshGraphics();
            } else {
                CreateGraphics(unit);
            }
        }

        private void CreateGraphics(Unit unit)
        {
            if (UnitGraphics.ContainsKey(unit))
                throw new ArgumentException("Unit already has graphics: " + unit.Properties.UniqueName);

            GameObject gameObject = PrefabManager.Instance.GetPrefabObject(unit.Stats.PrefabName);

            UnitGraphics.Add(unit, gameObject.GetComponent<UnitGraphics>());
            UnitGraphics[unit].unit = unit;
        }

        private void DestroyGraphics(Unit unit)
        {
            GameObject.Destroy(UnitGraphics[unit].gameObject);
            UnitGraphics.Remove(unit);
        }
    }

    // Use this for initialization
    void Start ()
    {
        cameraBase = GameObject.Find("HexMapCamera");

        mapEditorUi = GetComponentInChildren<HexMapEditor>(true);
        gameplayUi = GetComponentInChildren<GameplayUI>(true);
        currentUiMode = UIMode.Editor;

        SaveMapButton.onClick.AddListener(SaveMap);
        LoadMapButton.onClick.AddListener(LoadMap);
        CreateMapButton.onClick.AddListener(ShowCreateMapPanel);

        GameEventSystem.GameEvent += type =>
        {
            switch (type)
            {
                case GameEventSystem.GameEventType.TurnProcessed:
                    foreach (KeyValuePair<Unit, UnitBannerScript> banner in unitBanners)
                    {
                        banner.Value.UpdateUI();
                    }
                    break;
                case GameEventSystem.GameEventType.GameReloaded:
                    foreach (Unit unit in unitBanners.Keys) {
                        unitBanners[unit].DestroyUI();
                    }
                    unitBanners.Clear();

                    foreach (Unit unit in GameLogic.Instance.Units) {
                        CreateUnitBanner(unit);
                    }
                    break;
            }
        };
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
    }

    void SetUiMode(UIMode mode)
    {
        currentUiMode = mode;

        EditorUiGameObject.SetActive(currentUiMode == UIMode.Editor);
        GameUiGameObject.SetActive(currentUiMode == UIMode.Gameplay);
    }

    void CreateUnitBanner(Unit unit)
    {
        GameObject banner = GameObject.Instantiate(UnitBannerPrefab);
        unitBanners.Add(unit, banner.GetComponent<UnitBannerScript>());
        unitBanners[unit].Initialize(unit);
        unitBanners[unit].GetComponent<Button>().onClick.AddListener(() => {
            if (gameplayUi.gameObject.activeInHierarchy)
                gameplayUi.NotifyUnitBannerSelected(unit);
        });
    }

    void HandleMouse()
    {
        if (mouseCooldownCounter > 0)
            mouseCooldownCounter--;

        if (Input.GetMouseButton(0))
        {
            Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(inputRay, out hit))
            {
                HexCell currentCell = hexGrid.GetCell(hit.point);

                if (previousCell && previousCell != currentCell)
                {
                    ValidateDrag(currentCell);
                }
                else
                {
                    isDrag = false;
                }

                if (currentUiMode == UIMode.Editor)
                    mapEditorUi.EditCells(currentCell, isDrag, dragDirection);
                else
                    gameplayUi.CellClicked(currentCell, isDrag, dragDirection, 0);

                previousCell = currentCell;
                mouseCooldownCounter = MOUSE_HOLD_COOLDOWN;
            }
        }
        else
        {
            previousCell = null;
        }

        if (Input.GetMouseButtonDown(1))
        {
            Ray inputRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(inputRay, out hit))
            {
                HexCell currentCell = hexGrid.GetCell(hit.point);

                if (currentUiMode == UIMode.Gameplay)
                    gameplayUi.CellClicked(currentCell, isDrag, dragDirection, 1);

                if (Input.GetKey(KeyCode.LeftAlt))
                    GameLogic.Instance.CreateUnit(GameLogic.Instance.gameData.UnitTypes.ElementAt(0).Value, currentCell, HexDirection.SE, GameLogic.Instance.gameData.Factions.ElementAt(0).Value);
            }

            mouseCooldownCounter = MOUSE_HOLD_COOLDOWN;
        }
    }

    void HandleInput()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
            HandleMouse();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            SaveMap();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadMap();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (currentUiMode == UIMode.Editor)
                SetUiMode(UIMode.Gameplay);
            else
                SetUiMode(UIMode.Editor);
        }
    }

    void ValidateDrag(HexCell currentCell)
    {
        for (dragDirection = HexDirection.NE; dragDirection <= HexDirection.NW; dragDirection++)
        {
            if (previousCell.GetNeighbor(dragDirection) == currentCell)
            {
                isDrag = true;
                return;
            }
        }
        isDrag = false;
    }

    public void ShowCreateMapPanel()
    {
        CreateMapPanel.SetActive(true);
    }

    public void HideCreateMapPanel()
    {
        CreateMapPanel.SetActive(false);
    }

    public void CreateMap(int chunksX, int chunksY, bool generate, Texture2D heightmap = null)
    {
        HexGrid.Instance.Recreate(chunksX, chunksY);

        if(heightmap != null)
            HexGrid.Instance.ApplyHeightmap(heightmap, new RectInt(0, 0, heightmap.width, heightmap.height));

        IMapGenerator generator = new BasicGenerator();
        if(generate)
            generator.GenerateMap(HexGrid.Instance, (int)(UnityEngine.Random.value * int.MaxValue));

        HexGrid.Instance.RefreshAllCells();
        HexGrid.Instance.RebuildPathfinding();

        GameLogic.Instance.LoadGameState(null);
        gameplayUi.DeselectAll();
    }

    public void SaveMap()
    {
        SaveFileDialog dialog = new SaveFileDialog
        {
            FileName = "SavedMap", DefaultExt = "txt", Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*", InitialDirectory = "Data/Map/"
        };
        if (dialog.ShowDialog() == DialogResult.OK)
        {
            DataSaver.SaveMap(dialog.OpenFile(), hexGrid);
        }
    }

    public void LoadMap()
    {
        OpenFileDialog dialog = new OpenFileDialog()
        {
            DefaultExt = "txt", Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*", InitialDirectory = "Data/Map/"
        };
        if (dialog.ShowDialog() == DialogResult.OK)
        {
            DataLoader.LoadMap(dialog.OpenFile(), hexGrid);
        }
    }

    public void UpdateUnitGraphics()
    {
        foreach (Unit unit in GameLogic.Instance.Units) {
            if (unit.Alive) {
                if (!unitBanners.ContainsKey(unit)) {
                    CreateUnitBanner(unit);
                } else {
                    unitBanners[unit].UpdateUI();
                }

                UnitGraphicsManager.Instance.UpdateGraphics(unit);
            }
        }

        List<Unit> bannersToRemove = new List<Unit>();
        foreach (Unit key in unitBanners.Keys) {
            if (!key.Alive) {
                bannersToRemove.Add(key);
            }
        }

        foreach (Unit key in bannersToRemove) {
            unitBanners[key].DestroyUI();
            unitBanners.Remove(key);
        }

        UnitGraphicsManager.Instance.ClearGraphics();
    }
}
