﻿using System.Collections.Generic;
using Assets.Scripts.Game.Entities;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Game.Logic
{
    public class CombatStats
    {
        public Unit Combatant;

        public float Strength, Organization, Morale, Attack, Defence;
        public float StrengthRatio, OrganizationRatio, MoraleRatio;

        public CombatStats(Unit combatant)
        {
            Combatant = combatant;

            Attack = Combatant.Stats.Values[Unit.StatValues.Attack];
            Defence = Combatant.Stats.Values[Unit.StatValues.Defence];

            Strength = Combatant.Properties.Values[Unit.StatValues.Strength];
            Organization = Combatant.Properties.Values[Unit.StatValues.Organization];
            Morale = Combatant.Properties.Values[Unit.StatValues.Morale];

            StrengthRatio = Strength / Combatant.Stats.Values[Unit.StatValues.Strength];
            OrganizationRatio = Organization / Combatant.Stats.Values[Unit.StatValues.Organization];
            MoraleRatio = Morale / Combatant.Stats.Values[Unit.StatValues.Morale];
        }
    }

    public static class CombatUtils
    {
        public static bool CanAttack(Unit unit)
        {
            return unit.Properties.Values[Unit.StatValues.Organization] > 50f && unit.Properties.Values[Unit.StatValues.Morale] > 50f;
        }

        public static bool IsBattleOver(Battle battle, Dictionary<Unit, UnitLogicData> unitLogicData)
        {
            if (battle.Defenders.Count < 1 || battle.Attackers.Count < 1) {
                return true;
            }

            bool defendersFighting = false;
            bool livingDefenders = false;
            foreach (Unit defender in battle.Defenders) {
                if (defender.Alive) {
                    livingDefenders = true;

                    if (unitLogicData[defender].currentAction.Type == UnitAction.ActionType.Fight) {
                        defendersFighting = true;
                        break;
                    }
                }
            }

            bool attackersFighting = false;
            bool livingAttackers = false;
            foreach (Unit attacker in battle.Attackers) {
                if (attacker.Alive) {
                    livingAttackers = true;

                    if (unitLogicData[attacker].currentAction.Type == UnitAction.ActionType.Fight) {
                        attackersFighting = true;
                        break;
                    }
                }
            }

            return !(livingAttackers && livingDefenders && (defendersFighting || attackersFighting));
        }

        public static float ReduceDamageWithDefence(float damage, float defence)
        {
            if (defence > damage * 10f)
                return 1f;

            if (defence > 0.5f * damage) {
                damage = ReduceDamageWithDefence(damage * 0.5f, defence * 0.25f);
            } else {
                damage -= defence;
            }

            return damage;
        }

        // Checks wheter any of the combatants are capable to continue a fight in this turn
        public static bool CanBattleProgress(Battle battle, Dictionary<Unit, UnitLogicData> unitLogicData)
        {
            return !IsBattleOver(battle, unitLogicData);
        }

        public class CombatModifiers
        {
            public float minRoll;
            public float maxRoll;

            public float riverAttMult;
            public float riverDefMult;

            public float bridgeAttMult;
            public float bridgeDefMult;
        }

        static CombatModifiers defMults = new CombatModifiers {
            minRoll = 0.75f,
            maxRoll = 1.1f,

            riverAttMult = 0.5f,
            riverDefMult = 0.5f,
            bridgeAttMult = 0.5f,
            bridgeDefMult = 0.8f
        };

        static CombatModifiers attMults = new CombatModifiers {
            minRoll = 0.55f,
            maxRoll = 1.3f,

            riverAttMult = 0.75f,
            riverDefMult = 0.5f,
            bridgeAttMult = 0.75f,
            bridgeDefMult = 0.8f
        };

        public static void ProcessSingleAttackRound(Battle battle, Dictionary<Unit, UnitLogicData> unitLogicData, Unit defender, Unit attacker)
        {
            CombatStats defenderStats = new CombatStats(defender);
            CombatStats attackerStats = new CombatStats(attacker);

            int defenderElevationDifference = (defender.CurrentCell.Elevation - attacker.CurrentCell.Elevation);
            float heightAdvantageAttacker = defenderElevationDifference < 0 ? 1f + 0.15f * defenderElevationDifference : 1f;
            float heightAdvantageDefender = defenderElevationDifference > 0 ? 1f + 0.25f * defenderElevationDifference : 1f;

            attackerStats.Attack *= heightAdvantageAttacker;
            attackerStats.Defence *= heightAdvantageAttacker;
            defenderStats.Attack *= heightAdvantageDefender;
            defenderStats.Defence *= heightAdvantageDefender;

            if (defender.CurrentCell.HasRiver) {
                if (defender.CurrentCell.HasRoads) {
                    defenderStats.Defence *= defMults.bridgeDefMult;
                    defenderStats.Attack *= defMults.bridgeAttMult;
                } else {
                    defenderStats.Defence *= defMults.riverDefMult;
                    defenderStats.Attack *= defMults.riverAttMult;
                }
            }

            if (attacker.CurrentCell.HasRiver)
            {
                if (defender.CurrentCell.HasRoads) {
                    attackerStats.Defence *= attMults.bridgeDefMult;
                    attackerStats.Attack *= attMults.bridgeAttMult;
                } else {
                    attackerStats.Defence *= attMults.riverDefMult;
                    attackerStats.Attack *= attMults.riverAttMult;
                }
            }

            float AttackerSuccess = Random.Range(attMults.minRoll, attMults.maxRoll);
            float DefenderSuccess = Random.Range(defMults.minRoll, defMults.maxRoll);

            float attackerRoundDamage = AttackerSuccess * (attackerStats.Attack * 2f) * (attackerStats.Organization / 100f) * attackerStats.StrengthRatio;
            float attackerRoundDefence = ((attackerStats.Attack + attackerStats.Defence) * 0.5f) * ((attackerStats.Organization + 30f) / 130f);
            float defenderRoundDamage = DefenderSuccess * (defenderStats.Attack + defenderStats.Defence) * (defenderStats.Organization / 100f) * defenderStats.StrengthRatio;
            float defenderRoundDefence = defenderStats.Defence * ((defenderStats.Organization + 50f) / 150f);

            float attackerStrLoss = ReduceDamageWithDefence(defenderRoundDamage, attackerRoundDefence);
            float defenderStrLoss = ReduceDamageWithDefence(attackerRoundDamage, defenderRoundDefence);

            float casualtyRatio = Mathf.Clamp(defenderStrLoss / attackerStrLoss, 0.66f, 1.5f);
            float attackerStrRatioLoss = attackerStrLoss / attackerStats.Strength;
            float defenderStrRatioLoss = defenderStrLoss / defenderStats.Strength;

            float attackerOrgLoss = 5f + 50f * (1f / casualtyRatio) * attackerStrRatioLoss;
            float defenderOrgLoss = 3f + 50f * casualtyRatio * casualtyRatio * defenderStrRatioLoss;

            float attackerMoraleLoss = 2f + 100f * (1f / casualtyRatio) * attackerStrRatioLoss;
            float defenderMoraleLoss = 1f + 100f * casualtyRatio * casualtyRatio * defenderStrRatioLoss;

            if (unitLogicData[attacker].currentAction.Type == UnitAction.ActionType.Fight) {
                defender.Properties.Values[Unit.StatValues.Strength] -= defenderStrLoss;
                defender.Properties.Values[Unit.StatValues.Organization] -= defenderOrgLoss;
                defender.Properties.Values[Unit.StatValues.Morale] -= defenderMoraleLoss;
            }

            if (unitLogicData[defender].currentAction.Type == UnitAction.ActionType.Fight) {
                attacker.Properties.Values[Unit.StatValues.Strength] -= attackerStrLoss;
                attacker.Properties.Values[Unit.StatValues.Organization] -= attackerOrgLoss;
                attacker.Properties.Values[Unit.StatValues.Morale] -= attackerMoraleLoss;
            }
        }
    }

    public class Battle
    {
        public List<Unit> Defenders;
        public List<Unit> Attackers;
        public HexCell BattleCell;

        public Battle(HexCell cell)
        {
            BattleCell = cell;

            Defenders = new List<Unit>();
            Attackers = new List<Unit>();
        }
    }
}
