﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    public class UnitGraphics : MonoBehaviour
    {
        public Unit unit;

        // Use this for initialization
        void Start()
        {
            RefreshGraphics();
        }

        public void RefreshGraphics()
        {
            transform.localPosition = unit.CurrentCell.Position;
            transform.localRotation = Quaternion.LookRotation(unit.Facing.UnitVector());
        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}
