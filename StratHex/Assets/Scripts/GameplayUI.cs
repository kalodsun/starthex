﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;
using Assets.Scripts.Game.TiledMapPathfinder;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts
{
    class GameplayUI : MonoBehaviour
    {
        public GameObject PathMarkerPrefab;
        public HexGrid hexGrid;
        public GameScript gameScript;
        public UIScript UiScript;

        private HexCell selectedCell = null;
        private Unit selectedUnit = null;

        public GameObject turnButton;
        public GameObject selectedUnitNameText, selectedUnitAPText, selectedUnitStrText, selectedUnitOrgText, selectedUnitMoraleText;
        private List<GameObject> battleMarkers = new List<GameObject>();
        private List<GameObject> pathMarkers = new List<GameObject>();
        private readonly Color[] pathMarkerColorsByQueue = {Color.green, Color.blue, Color.red, Color.yellow, Color.cyan};

        public float lastTurnTime = 0f;
        public float turnInterval = 4f;
        public bool isGamePaused = true;

        private void UpdatePathMarkers()
        {
            foreach (GameObject pathMarker in pathMarkers)
                pathMarker.SetActive(false);

            if (selectedUnit != null)
            {
                List<Order> selectedUnitOrders = GameLogic.Instance.unitLogicData[selectedUnit].orders;

                int currentMarkerIndex = 0;
                int colorIndex = 0;
                for (int ordIndex = 0; ordIndex < selectedUnitOrders.Count; ordIndex++)
                {
                    Order order = selectedUnitOrders[ordIndex];

                    switch (order.Type)
                    {
                        case OrderType.Move:
                            HexMapPath path = GameLogic.Instance.unitLogicData[selectedUnit].paths[order];

                            if (path != null)
                            {
                                for (int i = 0; i < path.PathHexes.Count; i++)
                                {
                                    if (currentMarkerIndex >= pathMarkers.Count)
                                        pathMarkers.Add(GameObject.Instantiate(PathMarkerPrefab));

                                    GameObject curMarker = pathMarkers[currentMarkerIndex];

                                    curMarker.SetActive(true);
                                    //curMarker.transform.SetParent(path.PathHexes[i].transform);
                                    curMarker.transform.localPosition = path.PathHexes[i].Position + new Vector3(0, curMarker.transform.localScale.y / 2f, 0);
                                    curMarker.GetComponent<PathMarkerScript>().SetColor(pathMarkerColorsByQueue[colorIndex]);

                                    if (i + 1 < path.PathHexes.Count)
                                        curMarker.transform.rotation = Quaternion.LookRotation(HexUtils.GetDirection(path.PathHexes[i], path.PathHexes[i + 1]).UnitVector());

                                    currentMarkerIndex++;

                                    colorIndex = Mathf.FloorToInt(path.GetAccumulatedCost(i) / selectedUnit.Stats.Values[Unit.StatValues.Speed]) % pathMarkerColorsByQueue.Length;
                                }
                            }
                            break;
                        case OrderType.Bombard:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }

        private void UpdateUnitPanelUI()
        {
            if (selectedUnit != null)
            {
                selectedUnitNameText.GetComponent<Text>().text = selectedUnit.Properties.UniqueName + " (" + selectedUnit.Stats.VisibleName + ")";
                selectedUnitStrText.GetComponent<Text>().text = "Strength: " + selectedUnit.Properties.Values[Unit.StatValues.Strength].ToString("F0") + "/" + selectedUnit.Stats.Values[Unit.StatValues.Strength];
                selectedUnitOrgText.GetComponent<Text>().text = "Organization: " + selectedUnit.Properties.Values[Unit.StatValues.Organization].ToString("F0") + "/" + selectedUnit.Stats.Values[Unit.StatValues.Organization];
                selectedUnitMoraleText.GetComponent<Text>().text = "Morale: " + selectedUnit.Properties.Values[Unit.StatValues.Morale].ToString("F0") + "/" + selectedUnit.Stats.Values[Unit.StatValues.Morale];
            }
            else
            {
                selectedUnitNameText.GetComponent<Text>().text = "<No Selection>";
                selectedUnitAPText.GetComponent<Text>().text = "";
                selectedUnitStrText.GetComponent<Text>().text = "";
                selectedUnitOrgText.GetComponent<Text>().text = "";
                selectedUnitMoraleText.GetComponent<Text>().text = "";
            }
        }

        private void UpdateSelectionUI()
        {
            if (selectedUnit != null && !selectedUnit.Alive)
                selectedUnit = null;

            UpdatePathMarkers();
            UpdateUnitPanelUI();
        }

        public void DoNewTurnUpdate()
        {
            UpdateSelectionUI();
            SetUpBattleMarkers();
        }

        void NextTurn()
        {
            GameLogic.Instance.ProcessTurn();

            turnButton.GetComponentInChildren<Text>().text = "Turn " + GameLogic.Instance.TurnNumber;

            DoNewTurnUpdate();

            UiScript.UpdateUnitGraphics();
        }

        // Use this for initialization
        void Start()
        {
            /*turnButton = GameObject.Find("TurnButton");
            selectedUnitNameText = GameObject.Find("UnitNameText");
            selectedUnitAPText = GameObject.Find("UnitAPText");
            selectedUnitStrText = GameObject.Find("UnitStrText");
            selectedUnitOrgText = GameObject.Find("UnitOrgText");
            selectedUnitMoraleText = GameObject.Find("UnitMoraleText");*/

            turnButton.GetComponent<Button>().onClick.AddListener(NextTurn);
        }

        private void SetUpBattleMarkers()
        {
            HideBattleMarkers();

            Dictionary<HexCell, Battle> activeBattles = gameScript.gameLogic.activeBattles;
            Dictionary<Unit, UnitLogicData> unitLogicData = gameScript.gameLogic.unitLogicData;

            int activeMarkers = 0;
            foreach (KeyValuePair<HexCell, Battle> battle in activeBattles) {
                Vector3 markerPosition = battle.Value.BattleCell.Position;

                GameObject marker;
                if (activeMarkers >= battleMarkers.Count) {
                    marker = GameObject.Instantiate(UiScript.BattleMarkerPrefab);
                    battleMarkers.Add(marker);
                } else {
                    marker = battleMarkers[activeMarkers];
                }

                marker.SetActive(true);
                marker.transform.position = markerPosition;

                activeMarkers++;

                /*marker.gameObject.GetComponentInChildren<Button>().onClick.AddListener(() => {
                    UiScript.BattleInfoPanel.GetComponentInChildren<BattleInfoUI>().DisplayBattleInfo(battleHistory);
                });*/
            }
        }

        private void HideBattleMarkers()
        {
            foreach (GameObject battleMarker in battleMarkers) {
                battleMarker.SetActive(false);
            }
        }

        // Update is called once per frame
        void Update()
        {
            HandleInput();

            if (!isGamePaused && Time.time - lastTurnTime > turnInterval) {
                NextTurn();
                lastTurnTime = Time.time;
            }

            foreach (GameObject battleMarker in battleMarkers) {
                Vector3 oldAngles = battleMarker.transform.localEulerAngles;
                battleMarker.transform.localEulerAngles = new Vector3(oldAngles.x, UiScript.cameraBase.transform.localEulerAngles.y, oldAngles.z);
            }
        }

        public void DeselectAll()
        {
            selectedCell = null;
            selectedUnit = null;

            UpdateSelectionUI();
        }

        public void SelectAtCell(HexCell cell)
        {
            selectedCell = cell;
            selectedUnit = gameScript.gameLogic.GetUnitAtCell(cell);

            UpdateSelectionUI();
        }

        public void CellClicked(HexCell center, bool isDrag, HexDirection dragDirection, int mouseButton)
        {
            switch (mouseButton)
            {
                case 0:
                    DeselectAll();

                    SelectAtCell(center);
                    break;
                case 1:
                    if (selectedUnit != null)
                    {
                        if (!Input.GetKey(KeyCode.LeftShift))
                            gameScript.gameLogic.ClearOrders(selectedUnit);

                        if(center != selectedUnit.CurrentCell)
                            gameScript.gameLogic.QueueOrder(selectedUnit, Order.CreateMoveOrder(selectedUnit, center));
                    }
                    break;
            }

            UpdateSelectionUI();
        }

        void HandleInput()
        {
            if (Input.GetKeyDown(KeyCode.Space)) {
                isGamePaused = !isGamePaused;
            }

            if (Input.GetKeyDown(KeyCode.KeypadPlus)) {
                turnInterval /= 2;
            }

            if (Input.GetKeyDown(KeyCode.KeypadMinus)) {
                turnInterval *= 2;
            }

            Mathf.Clamp(turnInterval, 0.25f, 16f);
        }

        public void NotifyUnitBannerSelected(Unit unit)
        {
            SelectAtCell(unit.CurrentCell);
        }
    }
}
