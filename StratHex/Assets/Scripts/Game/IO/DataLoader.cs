﻿using System;
using System.Collections.Generic;
using System.IO;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;
using Assets.Scripts.Game.TiledMapGenerator;
using Boo.Lang.Runtime;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.Scripts.Game.IO
{
    static class DataLoader
    {
        private const string HEIGHTMAP_PATH = "Data/Textures/Heightmaps/";

        public static void LoadMap(string mapDataFile, HexGrid hexGrid, IMapGenerator mapGenerator = null)
        {
            LoadMap(File.OpenRead(mapDataFile), hexGrid, mapGenerator);
        } 

        public static void LoadMap(Stream stream, HexGrid hexGrid, IMapGenerator mapGenerator = null)
        {
            int SizeX, SizeZ;
            LoadMapMetadata(stream, out SizeX, out SizeZ);

            hexGrid.Recreate(SizeX, SizeZ);

            stream.Position = 0;

            LoadMapData(hexGrid, stream);

            if (mapGenerator != null)
                mapGenerator.GenerateMap(hexGrid, (int) (UnityEngine.Random.value * int.MaxValue));

            stream.Close();

            hexGrid.RefreshAllCells();
            hexGrid.RebuildPathfinding();
        }

        public static Dictionary<string, Faction> LoadFactions(string factionsDataFile)
        {
            Dictionary<string, Faction> factions = new Dictionary<string, Faction>();

            var reader = new StringReader(File.ReadAllText(factionsDataFile));

            Faction currentFaction = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    factions[currentFaction.Name] = currentFaction;
                    currentFaction = null;
                    continue;
                }

                if (words[0] == "{")
                    continue;

                if (currentFaction != null)
                {
                    switch (words[0])
                    {
                        case "VisibleName":
                            string name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i] + " ";
                            currentFaction.VisibleName = name;
                            break;
                        case "Parent":
                            //todo
                            break;
                        case "Color":
                            currentFaction.Color = Convert.ToUInt32(words[1], 16);
                            break;
                        default:
                            Debug.LogError("Invalid entry in faction data for " + currentFaction.Name + ": " + words[0]);
                            break;
                    }
                }
                else
                {
                    if (words[0] != "Relations")
                    {
                        currentFaction = new Faction();
                        currentFaction.Name = words[0];
                    }
                    else
                    {
                        ReadFactionRelations(reader, factions);
                    }
                }
            }

            return factions;
        }

        private static void ReadFactionRelations(StringReader reader, Dictionary<string, Faction> factions)
        {
            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    return;
                }

                if (words[0] == "{")
                {
                    continue;
                }

                if (!factions.ContainsKey(words[0]) || !factions.ContainsKey(words[1]))
                {
                    Debug.LogError("Invalid entry in faction relation data for " + words[0] + " / " + words[1]);
                    continue;
                }

                Faction.Relation relation = (Faction.Relation) Enum.Parse(typeof (Faction.Relation), words[2]);

                factions[words[0]].Relations.Add(factions[words[1]], relation);
                factions[words[1]].Relations.Add(factions[words[0]], relation);
            }
        }

        public static Dictionary<string, Structure.StatsCls> LoadStructures(string structureDataFile)
        {
            Dictionary<string, Structure.StatsCls> structures = new Dictionary<string, Structure.StatsCls>();

            var reader = new StringReader(File.ReadAllText(structureDataFile));

            Structure.StatsCls currentStructure = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    structures[currentStructure.Name] = currentStructure;
                    currentStructure = null;
                    continue;
                }

                if (words[0] == "{")
                    continue;

                if (currentStructure != null)
                {
                    string name;
                    switch (words[0])
                    {
                        case "InheritFrom":
                            if (!structures.ContainsKey(words[1]))
                                Debug.LogError("Invalid structure to inherit from in: " + currentStructure.Name + ": " + words[1]);

                            currentStructure.Inherit(structures[words[1]]);
                            break;
                        case "VisibleName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i] + " ";
                            currentStructure.VisibleName = name;
                            break;
                        case "PrefabName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i];
                            currentStructure.PrefabName = name;
                            break;
                        case "Directional":
                            if (words[1] == "yes")
                                currentStructure.Directional = true;
                            else if (words[1] == "no")
                                currentStructure.Directional = false;
                            else
                                Debug.LogError("Invalid value for structure - isDirectional in " + currentStructure.Name + ": " + words[1]);
                            break;
                        default:
                            Debug.LogError("Invalid entry in structure data for " + currentStructure.Name + ": " + words[0]);
                            break;
                    }
                }
                else
                {
                    currentStructure = new Structure.StatsCls();
                    currentStructure.Name = words[0];
                }
            }

            return structures;
        }

        public static Dictionary<string, Unit.StatsCls> LoadUnits(string unitDataFile)
        {
            Dictionary<string, Unit.StatsCls> units = new Dictionary<string, Unit.StatsCls>();

            var reader = new StringReader(File.ReadAllText(unitDataFile));

            Unit.StatsCls currentUnit = null;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    units[currentUnit.Name] = currentUnit;
                    currentUnit = null;
                    continue;
                }

                if (words[0] == "{")
                    continue;

                if (currentUnit != null)
                {
                    string name;
                    switch (words[0])
                    {
                        case "InheritFrom":
                            if (!units.ContainsKey(words[1]))
                                Debug.LogError("Invalid unit to inherit from in: " + currentUnit.Name + ": " + words[1]);

                            currentUnit.Inherit(units[words[1]]);
                            break;
                        case "VisibleName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i] + " ";
                            currentUnit.VisibleName = name;
                            break;
                        case "PrefabName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i];
                            currentUnit.PrefabName = name;
                            break;
                        case "CounterName":
                            name = "";
                            for (int i = 1; i < words.Length; i++)
                                name += words[i];
                            currentUnit.CounterName = name;
                            break;
                        default:
                            bool valueFound = false;
                            foreach (Unit.StatValues value in Enum.GetValues(typeof(Unit.StatValues)))
                            {
                                if (value.ToString().Equals(words[0]))
                                {
                                    valueFound = true;
                                    currentUnit.Values[value] = float.Parse(words[1]);
                                    break;
                                }
                            }

                            if (!valueFound)
                            {
                                Debug.LogError("Invalid entry in unit data for " + currentUnit.Name + ": " + words[0]);
                            }

                            break;
                    }
                }
                else
                {
                    currentUnit = new Unit.StatsCls();
                    currentUnit.Name = words[0];
                }
            }

            return units;
        }

        private static void LoadMapMetadata(Stream stream, out int SizeX, out int SizeZ)
        {
            var reader = new StreamReader(stream);

            SizeX = -1;
            SizeZ = -1;

            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                switch (words[0].ToLower())
                {
                    case "mapsize":
                        SizeX = Convert.ToInt32(words[1]);
                        SizeZ = Convert.ToInt32(words[2]);
                        break;
                }
            }
        }

        private enum MapDataBlock
        {
            Cells, None
        }

        private static void LoadMapData(HexGrid map, Stream stream)
        { 
            var reader = new StreamReader(stream);

            var currentBlock = MapDataBlock.None;
            string currentObject = null;

            HexCell curCell;
            while (reader.Peek() != -1)
            {
                string[] words = reader.ReadLine().Trim().Split();
                curCell = null;

                if (words[0] == "" || words[0][0] == '#')
                    continue;

                if (words[0] == "}")
                {
                    if (currentObject != null)
                    {
                        currentObject = null;
                    }
                    else
                        currentBlock = MapDataBlock.None;

                    continue;
                }

                if(words[0] == "{")
                    continue;

                if (currentBlock == MapDataBlock.None)
                {
                    switch (words[0].ToLower())
                    {
                        case "cells":
                            currentBlock = MapDataBlock.Cells;
                            break;
                        case "mapsize":
                        case "version":
                            break;
                        default:
                            Debug.LogError("Invalid block in map data: " + words[0]);
                            break;
                    }
                }
                else
                {
                    if (currentObject == null)
                    {
                        switch (currentBlock)
                        {
                            case MapDataBlock.Cells:
                                curCell = map.GetCellRawCoordinates(int.Parse(words[0]), int.Parse(words[1]));

                                curCell.TerrainType = (TerrainType)int.Parse(words[2]);
                                curCell.Elevation = int.Parse(words[3]);
                                curCell.WaterLevel = int.Parse(words[4]);

                                for(int i = 0;i < 6;i++)
                                    curCell.EdgeFeatures[i] = (EdgeFeature)int.Parse(words[5 + i]);

                                for (int i = 0; i < 6; i++)
                                    curCell.Roads[i] = (RoadType)int.Parse(words[11 + i]);

                                curCell.ForestLevel = int.Parse(words[17]);
                                curCell.RuralLevel = int.Parse(words[18]);
                                curCell.UrbanLevel = int.Parse(words[19]);
                                break;
                        }
                    }
                    else
                    {  
                        switch (currentBlock)
                        {
                            default:
                                break;
                        }
                    }
                }
            }
        }

        public static SavedGameState LoadGameState(Stream stream)
        {
            SavedGameState ret = new SavedGameState();

            return ret;
        }
    }
}
