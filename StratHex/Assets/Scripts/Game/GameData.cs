﻿using System.Collections.Generic;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;

namespace Assets.Scripts.Game
{
    public class GameData
    {
        public List<string> UnitTypeNames { get; private set; }

        public Dictionary<string, Structure.StatsCls> StructureTypes { get; private set; }
        public Dictionary<string, Unit.StatsCls> UnitTypes { get; private set; }
        public Dictionary<string, Faction> Factions { get; private set; }

        public GameData(Dictionary<string, Structure.StatsCls> structureTypes, Dictionary<string, Unit.StatsCls> unitTypes, Dictionary<string, Faction> factions)
        {
            StructureTypes = structureTypes;
            UnitTypes = unitTypes;
            Factions = factions;

            UnitTypeNames = new List<string>(unitTypes.Count);
            foreach(var key in unitTypes.Keys)
            {
                UnitTypeNames.Add(key);
            }
        }
    }
}
