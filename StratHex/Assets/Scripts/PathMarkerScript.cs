﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class PathMarkerScript : MonoBehaviour
    {
        public void SetColor(Color color)
        {
            foreach (MeshRenderer meshRenderer in gameObject.GetComponentsInChildren<MeshRenderer>())
            {
                meshRenderer.material.color = color;
            }
        }

        void Start()
        {
            
        }

        void Update()
        { 
        }
    }
}
