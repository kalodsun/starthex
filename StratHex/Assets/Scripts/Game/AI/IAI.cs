﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Game.AI
{
    public interface IComputerPlayer
    {
        void AnalyzeMap();

        void GiveOrders();
    }
}
