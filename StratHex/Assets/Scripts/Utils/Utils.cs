﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

// ReSharper disable once CheckNamespace
public static class Utils
{
    public static Color GetColorFromUint(uint uColor)
    {
        return new Color(uColor & 0x00ff0000, uColor & 0x0000ff00, uColor & 0x000000ff, uColor & 0xff000000);
    }

    public static GameObject GetMouseoverObject()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            return hit.collider.gameObject;
        }
        else
            return null;
    }

    public static Vector3 GetCameraCenterOnGround()
    {
        Ray centerRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0, 0.5f));
        return centerRay.origin - centerRay.direction * (centerRay.origin.y / centerRay.direction.y);
    }

    public static int[] SplitInRandomParts(int val, int parts)
    {
        if(parts <1)
            throw new ArgumentOutOfRangeException("Value can't be split in <1 parts");
        if(parts == 1)
            return new int[]{val};

        int[] randomIndecies = new int[parts+1];

        randomIndecies[0] = 0;
        for (int i = 1; i < parts; i++)
            randomIndecies[i] = Random.Range(0, val + 1);
        randomIndecies[parts] = val;

        Array.Sort(randomIndecies);

        int[] ret = new int[parts];

        for (int i = 0; i < parts; i++)
            ret[i] = randomIndecies[i + 1] - randomIndecies[i];

        return ret;
    }

    public static bool IsValidCoord(int val, int max)
    {
        return val >= 0 && val < max;
    }
}
