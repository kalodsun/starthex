﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class FpsDisplayScript : MonoBehaviour {

        private int fpsElapsedFrames = 0;
        private float fpsElapsedTime = 0;
        private Text FpsLabel;

        void Start ()
        {
            FpsLabel = gameObject.GetComponent<Text>();
        }
	
        void Update ()
        {
            fpsElapsedFrames++;
            fpsElapsedTime += Time.deltaTime;
            if (fpsElapsedTime > 0.25f) {
                FpsLabel.text = "FPS: " + Mathf.RoundToInt(fpsElapsedFrames * (1f / fpsElapsedTime));
                fpsElapsedTime = 0;
                fpsElapsedFrames = 0;
            }
        }
    }
}
