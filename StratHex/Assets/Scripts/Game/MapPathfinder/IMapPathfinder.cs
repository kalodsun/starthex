﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Assets.Scripts.Game.TiledMapPathfinder
{
    public delegate float EdgeCostDelegate(HexCell from, HexCell to);

    public class HexMapPath
    {
        public List<HexCell> PathHexes { get; private set; }
        private List<float> MovementCosts { get; set; }

        public void AddStep(HexCell cell, float accumulatedCost)
        {
            PathHexes.Add(cell);
            MovementCosts.Add(accumulatedCost);
        }

        public float GetSingleCost(int step)
        {
            return step == 0 ? MovementCosts[step] : MovementCosts[step] - MovementCosts[step - 1];
        }

        public float GetAccumulatedCost(int step)
        {
            return MovementCosts[step];
        }

        public HexMapPath()
        {
            PathHexes = new List<HexCell>();
            MovementCosts = new List<float>();
        }
    }

    public interface IMapPathfinder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>List of map tiles in the path, start and end inclusive, OR null if no path can be found</returns>
        [CanBeNull]
        HexMapPath FindPath(HexCell start, HexCell end, EdgeCostDelegate overrideDelegate = null);

        void SetDefaultEdgeCostDelegate(EdgeCostDelegate edgeCostDelegate);
    }
}
