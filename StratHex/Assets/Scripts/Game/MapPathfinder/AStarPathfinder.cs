﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;//only for Mathf

namespace Assets.Scripts.Game.TiledMapPathfinder
{
    class AStarPathfinder : IMapPathfinder
    {
        private readonly MapNode[,] graph;
        private readonly HexGrid map;
        private readonly Dictionary<MapNode, float>  searchDists;
        private EdgeCostDelegate edgeCostDelegate;

        public AStarPathfinder(HexGrid map, EdgeCostDelegate edgeCostDelegate)
        {
            this.map = map;
            this.edgeCostDelegate = edgeCostDelegate;
            graph = new MapNode[map.SizeX, map.SizeZ];

            for (int z = 0; z < map.SizeZ; z++)
                for (int x = 0; x < map.SizeX; x++)
                    graph[x, z] = new MapNode();

            for(int z = 0;z < map.SizeZ; z++)
                for (int x = 0; x < map.SizeX; x++)
                {
                    for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
                    {
                        HexCell neighbour = map.GetCellRawCoordinates(x, z).GetNeighbor(d);
                        if (neighbour == null)
                            continue;

                        try
                        {
                            graph[x, z].X = x;
                            graph[x, z].Y = z;
                            graph[x, z].MakeEdge(graph[neighbour.RawCoordinates.x, neighbour.RawCoordinates.y]);
                        }
                        catch (Exception ex)
                        {
                            ex.ToString();
                            throw;
                        }
                    }
                }

            searchDists = new Dictionary<MapNode, float>();
        }

        public void SetDefaultEdgeCostDelegate(EdgeCostDelegate edgeCostDelegate)
        {
            this.edgeCostDelegate = edgeCostDelegate;
        }

        public HexMapPath FindPath(HexCell start, HexCell end, EdgeCostDelegate overrideDelegate = null)
        {
            var path = FindPathNodes(graph[start.RawCoordinates.x, start.RawCoordinates.y], graph[end.RawCoordinates.x, end.RawCoordinates.y], overrideDelegate);

            if (path == null)
                return null;

            HexMapPath ret = new HexMapPath();

            foreach (MapNode node in path)
            {
                ret.AddStep(map.GetCellRawCoordinates(node.X, node.Y), searchDists[node]);
            }

            return ret;
        }

        private class FrontierComparer : IComparer<float>
        {
            public int Compare(float x, float y)
            {
                int result = x.CompareTo(y);

                if (result == 0)
                    return 1;   // Handle equality as beeing greater - nodes with the same weight should not overwrite each other
                return result;
            }
        }

        private float getHeurDist(MapNode a, MapNode b)
        {
            return HexCoordinates.FromOffsetCoordinates(a.X, a.Y).DistanceTo(HexCoordinates.FromOffsetCoordinates(b.X, b.Y));
        }

        private List<MapNode> FindPathNodes(MapNode start, MapNode end, EdgeCostDelegate overrideDelegate = null)
        {
            var visited = new HashSet<MapNode>();
            var prev = new Dictionary<MapNode, MapNode>();
            var frontier = new SortedList<float, MapNode>(new FrontierComparer());
            searchDists.Clear();

            searchDists[start] = 0;
            frontier.Add(getHeurDist(start, end), start);

            while(frontier.Count > 0)
            {
                var current = frontier.Values[0];
                visited.Add(current);
                frontier.RemoveAt(0);

                if (current == end)
                    return ReconstrutPath(current, start, prev);

                foreach(var edge in current.Connections)
                {
                    MapNode endNode = edge.End;
                    if (visited.Contains(endNode))
                        continue;

                    float edist = overrideDelegate == null ? edgeCostDelegate(edge.GetStartTile(map), edge.GetEndTile(map))
                                                            : overrideDelegate(edge.GetStartTile(map), edge.GetEndTile(map));

                    if(edist < 0)
                        continue;

                    float cdist = searchDists[current] + edist;

                    if (frontier.Values.Contains(endNode) && cdist >= searchDists[endNode])
                        continue;

                    prev[endNode] = current;
                    searchDists[endNode] = cdist;

                    if (frontier.Values.Contains(endNode))
                        frontier.RemoveAt(frontier.Values.IndexOf(endNode));

                    float heurDist = getHeurDist(endNode, end);
                    frontier.Add(searchDists[endNode] + heurDist, endNode);
                }

            }

            return null;
        }

        private static List<MapNode> ReconstrutPath(MapNode end, MapNode start, Dictionary<MapNode, MapNode> prev)
        {
            var ret = new List<MapNode> {end};

            while (end != start)
            {
                end = prev[end];
                ret.Add(end);
            }

            ret.Reverse();

            return ret;
        }

        internal class MapNode
        {
            public int X, Y;
            public readonly List<MapEdge> Connections;

            public MapNode()
            {
                Connections = new List<MapEdge>();
            }

            public bool HasEdgeTo(MapNode node)
            {
                return Connections.Any(edge => edge.End == node);
            }

            public MapEdge GetEdge(MapNode node)
            {
                return Connections.FirstOrDefault(edge => edge.End == node);
            }

            public void MakeEdge(MapNode end)
            {
                Connections.Add(new MapEdge(this, end));
            }

            public void DeleteEdge(MapNode end)
            {
                Connections.Remove(GetEdge(end));
            }

            internal class MapEdge
            {
                public MapNode Start, End;

                public MapEdge(MapNode start, MapNode end)
                {
                    Start = start;
                    End = end;
                }

                public HexCell GetStartTile(HexGrid map)
                {
                    return map.GetCellRawCoordinates(Start.X, Start.Y);
                }

                public HexCell GetEndTile(HexGrid map)
                {
                    return map.GetCellRawCoordinates(End.X, End.Y);
                }
            }
        }
    }
}
