﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts
{
    public class HexFeatureManager : MonoBehaviour {
        public HexFeatureCollection[] urbanCollections, ruralCollections, forestCollections;
        public Transform bridge;
        private Transform container;
        private List<GameObject> combinedMeshes = new List<GameObject>();

        private Dictionary<Material, List<CombineInstance>> combineInstances = new Dictionary<Material, List<CombineInstance>>();

        private const int MAX_FEATURES_PER_TILE = 21;
        private const int FEATURES_PER_LEVEL = MAX_FEATURES_PER_TILE / 3;
        private const float RIVER_HALF_WIDTH_RATIO = 0.25f;// TODO take it from a central place

        // arrays correspond to feature levels
        // used to select big/medium/small feature
        private static float[][] featureThresholds =
        {
            new float[] {0.0f, 0.0f, 0.8f},
            new float[] {0.0f, 0.5f, 0.9f},
            new float[] {0.4f, 0.8f, 1f}
        };

        public void AddBridge(Vector3 roadCenter1, Vector3 roadCenter2)
        {
            roadCenter1 = HexUtils.Perturb(roadCenter1, HexUtils.perturbSterngth);
            roadCenter2 = HexUtils.Perturb(roadCenter2, HexUtils.perturbSterngth);

            Transform instance = Instantiate(bridge);
            instance.localPosition = (roadCenter1 + roadCenter2) * 0.5f;
            instance.localRotation = Quaternion.LookRotation(roadCenter2 - roadCenter1, Vector3.up);
            instance.SetParent(container, false);
        }

        Transform PickPrefab(HexFeatureCollection[] collection, int featureLevel, float featureSizeHash, float featureIndexHash)
        {
            if (featureLevel > 0)
            {
                float[] thresholds = featureThresholds[featureLevel - 1];
                for (int i = 0; i < thresholds.Length; i++)
                {
                    if (featureSizeHash < thresholds[i])
                    {
                        return collection[i].Pick(featureIndexHash);
                    }
                }
            }
            return null;
        }

        public void Clear()
        {
            foreach (GameObject meshObject in combinedMeshes)
            {
                Destroy(meshObject.GetComponent<MeshFilter>().mesh);
                Destroy(meshObject);
            }
            combinedMeshes.Clear();

            if (container)
            {
                Destroy(container.gameObject);
            }
            container = new GameObject("Features Container").transform;
            container.SetParent(transform, false);
        }

        public void Apply()
        {
            foreach (Material material in combineInstances.Keys) {
                GameObject meshObject = new GameObject();
                meshObject.AddComponent<MeshFilter>();
                meshObject.GetComponent<MeshFilter>().mesh = new Mesh();
                meshObject.GetComponent<MeshFilter>().mesh.indexFormat = IndexFormat.UInt32;

                meshObject.GetComponent<MeshFilter>().mesh.CombineMeshes(combineInstances[material].ToArray());
                meshObject.SetActive(true);
                meshObject.transform.SetParent(container, false);

                meshObject.AddComponent<MeshRenderer>();
                meshObject.GetComponent<MeshRenderer>().sharedMaterial = material;
                meshObject.GetComponent<MeshRenderer>().shadowCastingMode = ShadowCastingMode.On;

                combinedMeshes.Add(meshObject);
            }

            combineInstances.Clear();
        }

        private static bool isOverRoadOrRiver(Vector2 coord, HexCell cell, float radius)
        {
            coord -= new Vector2(cell.Position.x, cell.Position.z);


            for (int dir = 0; dir < 6; dir++) {
                if (cell.HasRiverThroughEdge((HexDirection) dir) || cell.HasRoadThroughEdge((HexDirection) dir)) {
                    float effectiveRadius = cell.HasRiverThroughEdge((HexDirection)dir) ? radius : (0.75f * radius);

                    Vector3 dirVector3 = ((HexDirection) dir).UnitVector();
                    Vector2 dirVector = new Vector2(dirVector3.x, dirVector3.z);
                    Vector2 rectHeight = dirVector;

                    Vector2 baseA = new Vector2(-dirVector.y, dirVector.x) * RIVER_HALF_WIDTH_RATIO * cell.Grid.cellSize;
                    Vector2 baseB = new Vector2(dirVector.y, -dirVector.x) * RIVER_HALF_WIDTH_RATIO * cell.Grid.cellSize;
                    rectHeight *= cell.Grid.cellSize * 2;

                    if (radius > 0.001f) {
                        baseA += baseA.normalized * effectiveRadius;
                        baseB += baseB.normalized * effectiveRadius;
                        rectHeight += rectHeight.normalized * effectiveRadius;
                    }

                    rectHeight += baseB;

                    if (radius > 0.001f) {
                        baseA += (dirVector.normalized * effectiveRadius * -1f);
                        baseB += (dirVector.normalized * effectiveRadius * -1f);
                    }

                    Vector2 rectBase = baseB - baseA;
                    Vector2 diagA = coord - baseA;
                    Vector2 diagB = coord - baseB;

                    if ((0 <= Vector2.Dot(rectBase, diagA)) && (Vector2.Dot(rectBase, diagA) <= Vector2.Dot(rectBase, rectBase)) &&
                        (0 <= Vector2.Dot(rectHeight, diagB)) && (Vector2.Dot(rectHeight, diagB) <= Vector2.Dot(rectHeight, rectHeight)))
                        return true;
                }
            }

            return false;
        }

        private static bool isObstructed(Vector2 coord, Vector2[] takenCoords, float[] takenRadii, int takenCount, float radius)
        {
            for (int i = 0; i < takenCount; i++) {
                if (Vector2.Distance(coord, takenCoords[i]) < (takenRadii[i] + radius))
                    return true;
            }

            return false;
        }

        public void AddFeatures(HexCell cell)
        {
            HexHash hash = HexUtils.SampleHashGrid(cell.Position);

            Random.InitState(Mathf.RoundToInt(int.MaxValue * hash.a));

            int[] levels = {cell.ForestLevel, cell.RuralLevel, cell.UrbanLevel};
            HexFeatureCollection[][] collections = { forestCollections, ruralCollections , urbanCollections};

            int takenPositionsCount = 0;
            Vector2[] takenPositions = new Vector2[MAX_FEATURES_PER_TILE];
            float[] takenRadii = new float[MAX_FEATURES_PER_TILE];

            for (int type = 0; type < 3; type++) {
                /*int obstructedSides = 0;
                for (int i = 0; i < 6; i++) {
                    if (cell.HasRiverThroughEdge((HexDirection) i) || cell.HasRoadThroughEdge((HexDirection) i))
                        obstructedSides++;
                }*/

                float targetCount = Mathf.FloorToInt(FEATURES_PER_LEVEL * levels[type]);// * ((6f - obstructedSides) / 6f));

                int attemptsCount = 0;
                for (int i = 0; i < targetCount; i++) {
                    Transform featurePrefab = PickPrefab(collections[type], levels[type], Random.value, Random.value);

                    if(featurePrefab == null)
                        continue;

                    HexFeatureDescription featureDescription = featurePrefab.GetComponent<HexFeatureDescription>();

                    float radius = 1f, yDisplacement = 0f;
                    if (featureDescription != null) {
                        radius = featureDescription.Radius;
                        yDisplacement = featureDescription.YDisplacement;
                    }

                    Vector3 featureCoord = Vector3.zero;
                    bool coordValid = false;

                    do {
                        attemptsCount++;

                        HexDirection direction = (HexDirection)Mathf.FloorToInt(Random.value * 6);

                        featureCoord = cell.Position + new Vector3(0, yDisplacement, 0) + HexUtils.GetRandomPosInTriangle(
                            Vector3.zero, HexUtils.GetFirstSolidCorner(direction, 0.75f, cell.Grid.cellSize), HexUtils.GetSecondSolidCorner(direction, 0.75f, cell.Grid.cellSize), Random.value, Random.value);

                        if (!isObstructed(new Vector2(featureCoord.x, featureCoord.z), takenPositions, takenRadii, takenPositionsCount, radius) && !isOverRoadOrRiver(new Vector2(featureCoord.x, featureCoord.z), cell, radius * 0.75f)) {
                            coordValid = true;
                            break;
                        }
                    } while (attemptsCount < MAX_FEATURES_PER_TILE * 5);


                    if(!coordValid)
                        continue;

                    targetCount -= 1 * Mathf.Sqrt(radius);
                    takenPositions[takenPositionsCount] = new Vector2(featureCoord.x, featureCoord.z);
                    takenRadii[takenPositionsCount] = radius;
                    takenPositionsCount++;

                    Material material = featurePrefab.GetComponent<MeshRenderer>().sharedMaterial;
                    if (!combineInstances.ContainsKey(material)) {
                        combineInstances.Add(material, new List<CombineInstance>());
                    }

                    CombineInstance combineInstance = new CombineInstance();
                    combineInstance.mesh = featurePrefab.GetComponent<MeshFilter>().sharedMesh;
                    combineInstance.transform = Matrix4x4.TRS(featureCoord, Quaternion.Euler(0f, 360f * Random.value, 0f), Vector3.one) * featurePrefab.transform.localToWorldMatrix;
                    combineInstances[material].Add(combineInstance);
                }
            }
        }
    }

    [System.Serializable]
    public struct HexFeatureCollection
    {
        public Transform[] prefabs;

        public Transform Pick(float choice)
        {
            return prefabs[(int)(choice * prefabs.Length)];
        }
    }
}
