﻿using System;
using System.IO;
using UnityEngine;

namespace Assets.Scripts
{
    public enum EdgeFeature
    {
        None, RiverOutgoing, RiverIncoming
    }

    public enum RoadType
    {
        None, Dirt, Standart, Rail
    }

    public enum TerrainType
    {
        Sand, Grass, DryGrass, Rock, Snow, Urban
    }

    public class HexCell : MonoBehaviour {
        [SerializeField]
        private HexCell[] neighbors;

        public HexCoordinates coordinates;
        public RectTransform uiRect;

        public HexGridChunk chunk;

        public IntVector2 RawCoordinates { get; set; }

        private int elevation;
        public int Elevation
        {
            get
            {
                return elevation;
            }
            set
            {
                if (elevation == value)
                    return;

                elevation = value;
                Vector3 position = transform.localPosition;
                position.y = value * Grid.elevationStep;
                transform.localPosition = position;

                Vector3 uiPosition = uiRect.localPosition;
                uiPosition.z = elevation * -Grid.elevationStep;
                uiRect.localPosition = uiPosition;
                
                for (int i = 0; i < Roads.Length; i++)
                {
                    if (Roads[i] != RoadType.None && GetElevationDifference((HexDirection)i) > 1)
                    {
                        SetRoad((HexDirection)i, RoadType.None);
                    }
                }

                this.Refresh();
            }
        }

        int waterLevel;
        public int WaterLevel
        {
            get
            {
                return waterLevel;
            }
            set
            {
                if (waterLevel == value)
                {
                    return;
                }
                waterLevel = value;
                Refresh();
            }
        }

        int urbanLevel, ruralLevel, forestLevel;
        public int UrbanLevel
        {
            get
            {
                return urbanLevel;
            }
            set
            {
                if (urbanLevel != value)
                {
                    urbanLevel = value;
                    Refresh(false);
                }
            }
        }

        public int RuralLevel
        {
            get
            {
                return ruralLevel;
            }
            set
            {
                if (ruralLevel != value)
                {
                    ruralLevel = value;
                    Refresh(false);
                }
            }
        }

        public int ForestLevel
        {
            get
            {
                return forestLevel;
            }
            set
            {
                if (forestLevel != value)
                {
                    forestLevel = value;
                    Refresh(false);
                }
            }
        }


        public bool IsUnderwater
        {
            get
            {
                return waterLevel > elevation;
            }
        }

        public float StreamBedY
        {
            get
            {
                return (elevation + chunk.riverbedElevationOffset) * Grid.elevationStep;
            }
        }

        public float RiverSurfaceY
        {
            get
            {
                return (elevation + chunk.waterSurfaceElevationOffset) * Grid.elevationStep;
            }
        }

        public float WaterSurfaceY
        {
            get
            {
                return
                    (waterLevel + chunk.waterSurfaceElevationOffset) * Grid.elevationStep;
            }
        }

        public int GetElevationDifference(HexDirection direction)
        {
            int difference = elevation - GetNeighbor(direction).elevation;
            return difference >= 0 ? difference : -difference;
        }

        public Vector3 Position
        {
            get
            {
                return transform.localPosition;
            }
        }

        private TerrainType terrainType;
        public TerrainType TerrainType {
            get { return terrainType; }

            set
            {
                if(terrainType == value)
                    return;

                terrainType = value;
                Refresh();
            }

        }

        public Color Color
        {
            get
            {
                return HexUtils.TerrainColors[TerrainType];
            }
        }

        public EdgeFeature[] EdgeFeatures = new EdgeFeature[6];

        public RoadType[] Roads = new RoadType[6];

        public HexGrid Grid { get; set; }

        public HexCell GetNeighbor(HexDirection direction)
        {
            return neighbors[(int)direction];
        }

        private void Refresh(bool refreshNeghbours = true)
        {
            if(chunk)
                chunk.Refresh();

            for (int i = 0; i < neighbors.Length && refreshNeghbours; i++)
            {
                HexCell neighbor = neighbors[i];
                if (neighbor != null && neighbor.chunk != chunk)
                {
                    neighbor.Refresh(false);
                }
            }
        }

        public void SetNeighbor(HexDirection direction, HexCell cell)
        {
            neighbors[(int)direction] = cell;
            cell.neighbors[(int)direction.Opposite()] = this;
        }

        public HexEdgeType GetEdgeType(HexDirection direction)
        {
            return HexUtils.GetEdgeType(elevation, neighbors[(int)direction].elevation);
        }

        public HexEdgeType GetEdgeType(HexCell otherCell)
        {
            return HexUtils.GetEdgeType(elevation, otherCell.elevation);
        }

        public bool HasRoadThroughEdge(HexDirection direction)
        {
            return Roads[(int)direction] != RoadType.None;
        }

        public bool HasRoads
        {
            get
            {
                for (int i = 0; i < Roads.Length; i++)
                {
                    if (Roads[i] != RoadType.None)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool HasRiver
        {
            get
            {
                foreach (HexDirection value in Enum.GetValues(typeof (HexDirection)))
                {
                    if (HasRiverThroughEdge(value))
                        return true;
                }
                return false;
            }
        }

        public bool HasRiverThroughEdge(HexDirection direction)
        {
            return EdgeFeatures[(int) direction] == EdgeFeature.RiverIncoming ||
                   EdgeFeatures[(int) direction] == EdgeFeature.RiverOutgoing;
        }

        public bool HasRiverBeginOrEnd
        {
            get
            {
                int counter = 0;
                foreach (HexDirection value in Enum.GetValues(typeof(HexDirection)))
                {
                    if (HasRiverThroughEdge(value))
                        counter++;
                }
                return counter == 1;
            }
        }

        public void RemoveRoad(HexDirection direction)
        {
            Roads[(int)direction] = RoadType.None;

            var neighbour = GetNeighbor(direction);
            neighbour.Roads[(int)direction.Opposite()] = RoadType.None;

            Refresh(false);
            neighbour.Refresh(false);
        }

        public void SetRoad(HexDirection direction, RoadType type)
        {
            if (Roads[(int)direction] == type || HasRiverThroughEdge(direction) || GetEdgeType(direction) == HexEdgeType.Cliff)
                return;

            Roads[(int)direction] = type;

            var neighbour = GetNeighbor(direction);
            neighbour.Roads[(int)direction.Opposite()] = type;

            Refresh(false);
            neighbour.Refresh(false);
        }

        public void RemoveRiver(HexDirection direction)
        {
            EdgeFeatures[(int)direction] = EdgeFeature.None;

            var neighbour = GetNeighbor(direction);
            neighbour.EdgeFeatures[(int)direction.Opposite()] = EdgeFeature.None;

            Refresh(false);
            neighbour.Refresh(false);
        }

        public void AddRiver(HexDirection direction, bool outgoing)
        {
            EdgeFeature n = outgoing ? EdgeFeature.RiverOutgoing : EdgeFeature.RiverIncoming, nf = outgoing ? EdgeFeature.RiverIncoming : EdgeFeature.RiverOutgoing;

            EdgeFeatures[(int)direction] = n;

            var neighbour = GetNeighbor(direction);
            neighbour.EdgeFeatures[(int)direction.Opposite()] = nf;

            SetRoad(direction, RoadType.None);

            Refresh(false);
            neighbour.Refresh(false);
        }
    }
}
