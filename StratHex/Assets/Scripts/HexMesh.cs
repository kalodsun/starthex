﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public struct EdgeVertices
    {
        public const int vertexCount = 5;
        public Vector3[] v;

        public EdgeVertices(Vector3 corner1, Vector3 corner2)
        {
            v = new Vector3[vertexCount];

            v[0] = corner1;
            for(float i = 1;i < vertexCount - 1;i++)
                v[(int)i] = Vector3.Lerp(corner1, corner2, i / (vertexCount - 1));
            
            v[vertexCount-1] = corner2;
        }

        public EdgeVertices(Vector3 corner1, Vector3 corner2, float outerStep)
        {
            v = new Vector3[vertexCount];

            v[0] = corner1;
            v[vertexCount - 1] = corner2;

            v[1] = Vector3.Lerp(corner1, corner2, outerStep);
            v[vertexCount - 2] = Vector3.Lerp(corner1, corner2, 1f - outerStep);

            corner1 = v[1];
            corner2 = v[vertexCount - 2];

            for (float i = 2; i < vertexCount - 2; i++)
                v[(int) i] = Vector3.Lerp(corner1, corner2, i / (vertexCount - 1));
        }
    }

    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class HexMesh : MonoBehaviour
    {

        private Mesh hexMesh;
        private MeshCollider meshCollider;
        public bool useCollider, useColors, useUVCoordinates;

        [NonSerialized]
        private List<Vector3> vertices;
        [NonSerialized]
        private List<Color> colors;
        [NonSerialized]
        private List<int> triangles;
        [NonSerialized]
        private List<Vector2> uvs;

        void Awake()
        {
            GetComponent<MeshFilter>().mesh = hexMesh = new Mesh();
            if (useCollider)
            {
                meshCollider = gameObject.AddComponent<MeshCollider>();
            }
            hexMesh.name = "Terrain";
        }

        public void Clear()
        {
            hexMesh.Clear();
            vertices = ListPool<Vector3>.Get();

            if (useColors)
                colors = ListPool<Color>.Get();

            triangles = ListPool<int>.Get();

            if (useUVCoordinates)
            {
                uvs = ListPool<Vector2>.Get();
            }
        }

        public void Apply()
        {
            hexMesh.SetVertices(vertices);
            ListPool<Vector3>.Add(vertices);

            if (useColors)
            {
                hexMesh.SetColors(colors);
                ListPool<Color>.Add(colors);
            }

            hexMesh.SetTriangles(triangles, 0);
            ListPool<int>.Add(triangles);

            if (useUVCoordinates)
            {
                hexMesh.SetUVs(0, uvs);
                ListPool<Vector2>.Add(uvs);
            }

            hexMesh.RecalculateNormals();

            if (useCollider)
            {
                meshCollider.sharedMesh = hexMesh;
            }
        }

        public void AddTriangle(Vector3 v1, Vector3 v2, Vector3 v3, bool perturb = true)
        {
            int vertexIndex = vertices.Count;

            if (perturb)
            {
                v1 = HexUtils.Perturb(v1, HexUtils.perturbSterngth);
                v2 = HexUtils.Perturb(v2, HexUtils.perturbSterngth);
                v3 = HexUtils.Perturb(v3, HexUtils.perturbSterngth);
            }

            vertices.Add(v1);
            vertices.Add(v2);
            vertices.Add(v3);

            triangles.Add(vertexIndex);
            triangles.Add(vertexIndex + 1);
            triangles.Add(vertexIndex + 2);
        }

        public void AddTriangleColor(Color color)
        {
            colors.Add(color);
            colors.Add(color);
            colors.Add(color);
        }

        public void AddTriangleColor(Color c1, Color c2, Color c3)
        {
            colors.Add(c1);
            colors.Add(c2);
            colors.Add(c3);
        }

        public void AddQuad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, bool perturb = true)
        {
            int vertexIndex = vertices.Count;

            if (perturb)
            {
                v1 = HexUtils.Perturb(v1, HexUtils.perturbSterngth);
                v2 = HexUtils.Perturb(v2, HexUtils.perturbSterngth);
                v3 = HexUtils.Perturb(v3, HexUtils.perturbSterngth);
                v4 = HexUtils.Perturb(v4, HexUtils.perturbSterngth);
            }

            vertices.Add(v1);
            vertices.Add(v2);
            vertices.Add(v3);
            vertices.Add(v4);

            triangles.Add(vertexIndex);
            triangles.Add(vertexIndex + 2);
            triangles.Add(vertexIndex + 1);
            triangles.Add(vertexIndex + 1);
            triangles.Add(vertexIndex + 2);
            triangles.Add(vertexIndex + 3);
        }

        public void AddQuadColor(Color c1, Color c2, Color c3, Color c4)
        {
            colors.Add(c1);
            colors.Add(c2);
            colors.Add(c3);
            colors.Add(c4);
        }

        public void AddQuadColor(Color c1, Color c2)
        {
            AddQuadColor(c1, c1, c2, c2);
        }

        public void AddQuadColor(Color c1)
        {
            AddQuadColor(c1, c1);
        }

        public void AddTriangleUV(Vector2 uv1, Vector2 uv2, Vector3 uv3)
        {
            uvs.Add(uv1);
            uvs.Add(uv2);
            uvs.Add(uv3);
        }

        public void AddQuadUV(Vector2 uv1, Vector2 uv2, Vector3 uv3, Vector3 uv4)
        {
            uvs.Add(uv1);
            uvs.Add(uv2);
            uvs.Add(uv3);
            uvs.Add(uv4);
        }

        public void AddQuadUV(float uMin, float uMax, float vMin, float vMax)
        {
            uvs.Add(new Vector2(uMin, vMin));
            uvs.Add(new Vector2(uMax, vMin));
            uvs.Add(new Vector2(uMin, vMax));
            uvs.Add(new Vector2(uMax, vMax));
        }
    }
}
