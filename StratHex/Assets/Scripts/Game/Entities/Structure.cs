﻿namespace Assets.Scripts.Game.Entities
{
    public class Structure
    {
        public Structure(StatsCls stats, HexCell cell)
        {
            Stats = stats;
            Properties = new PropertiesCls(stats);
            this.cell = cell;
        }

        private readonly HexCell cell;

        public HexDirection Direction { get; set; }

        public HexCell Cell
        {
            get { return cell; }
        }

        public PropertiesCls Properties { private set; get; }
        public StatsCls Stats { private set; get; }

        public class PropertiesCls
        {
            public PropertiesCls(StatsCls stats)
            {
                
            }
        }

        public class StatsCls
        {
            public string PrefabName;
            public string Name = "BasicStructure";
            public string VisibleName = "NOVISIBLENAME";

            public bool Directional = false;

            public void Inherit(StatsCls original)
            {
                PrefabName = original.PrefabName;
                VisibleName = original.VisibleName;
                Directional = original.Directional;
            }
        }
    }
}
