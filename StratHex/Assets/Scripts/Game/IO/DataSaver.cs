﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;

namespace Assets.Scripts.Game.IO
{
    class DataSaver
    {
        public static void SaveGameState(Stream fileStream, SavedGameState gameState)
        {
            StreamWriter writer = new StreamWriter(fileStream);

            writer.WriteLine("Version " + UnityEngine.Application.version);

            writer.WriteLine("Units");
            writer.WriteLine("{");
            foreach (Unit unit in gameState.Units) {
                WriteUnitData(writer, unit);
            }
            writer.WriteLine("}");

            writer.Flush();
            writer.Close();
        }

        private static void WriteUnitData(StreamWriter writer, Unit unit)
        {
            StringBuilder stringBuilder = new StringBuilder();

            writer.WriteLine(stringBuilder.ToString());
        }

        public static void SaveMap(Stream fileStream, HexGrid map)
        {
            StreamWriter writer = new StreamWriter(fileStream);

            writer.WriteLine("Version " + UnityEngine.Application.version);
            writer.WriteLine("MapSize " + map.SizeX + " " + map.SizeZ);

            writer.WriteLine("Cells");
            writer.WriteLine("{");
            for (int x = 0; x < map.SizeX; x++)
            {
                for (int z = 0; z < map.SizeZ; z++)
                {
                    HexCell cell = map.GetCellRawCoordinates(x, z);

                    StringBuilder cellString = new StringBuilder("\t" + x + " " + z + " " + (int)cell.TerrainType + " " + cell.Elevation.ToString() + " " + cell.WaterLevel);

                    for (int i = 0; i < 6; i++)
                        cellString.Append(" " + (int)cell.EdgeFeatures[i]);

                    for (int i = 0; i < 6; i++)
                        cellString.Append(" " + (int)cell.Roads[i]);

                    cellString.Append(" " + cell.ForestLevel + " " + cell.RuralLevel + " " + cell.UrbanLevel);

                    writer.WriteLine(cellString.ToString());
                }
            }
            writer.WriteLine("}");

            writer.Flush();
            writer.Close();
        }

        public static void SaveMap(string mapDataFile, HexGrid map)
        {
            if(File.Exists(mapDataFile))
                File.Delete(mapDataFile);
            
            SaveMap(File.OpenWrite(mapDataFile), map);
        }
    }
}
