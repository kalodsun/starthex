﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public static class HexUtils
    {
        public static Texture2D noiseSource;

        private static float radiusRatio = 0.866025404f;
        public const int terracesPerSlope = 2;
        public const int terraceSteps = terracesPerSlope * 2 + 1;
        public const float noiseScale = 0.003f;
        public const float perturbSterngth = 2.5f;

        public static readonly Color RiverColor = new Color(194, 227, 248);

        public const float horizontalTerraceStepSize = 1f / terraceSteps;
        public const float verticalTerraceStepSize = 1f / (terracesPerSlope + 1);

        public static readonly Dictionary<TerrainType, Color> TerrainColors = new Dictionary<TerrainType, Color>();

        static HexUtils()
        {
            TerrainColors.Add(TerrainType.Sand, new Color(238f/255, 202f/255, 144f/255));
            TerrainColors.Add(TerrainType.Grass, new Color(72f/255, 101f/255, 45f/255));
            TerrainColors.Add(TerrainType.DryGrass, new Color(142f/255, 129f/255, 36f/255));
            TerrainColors.Add(TerrainType.Urban, new Color(122f/255, 121f/255, 119f/255));
            TerrainColors.Add(TerrainType.Snow, new Color(255f/255, 255f/255, 255f/255));
            TerrainColors.Add(TerrainType.Rock, new Color(156f/255, 156f/255, 161f/255));
        }

        public static Vector4 SampleNoise(Vector3 position)
        {
            return noiseSource.GetPixelBilinear(position.x * noiseScale, position.z * noiseScale);
        }

        public static Vector3 TerraceLerp(Vector3 a, Vector3 b, int step)
        {
            float h = step * horizontalTerraceStepSize;
            a.x += (b.x - a.x) * h;
            a.z += (b.z - a.z) * h;
            float v = ((step + 1) / 2) * verticalTerraceStepSize;
            a.y += (b.y - a.y) * v;
            return a;
        }

        public static EdgeVertices TerraceLerp(EdgeVertices a, EdgeVertices b, int step)
        {
            EdgeVertices result;

            result.v = new Vector3[EdgeVertices.vertexCount];
            for (int i = 0; i < EdgeVertices.vertexCount; i++)
            {
                result.v[i] = TerraceLerp(a.v[i], b.v[i], step);
            }
            return result;
        }

        public static Color TerraceLerp(Color a, Color b, int step)
        {
            float h = step * horizontalTerraceStepSize;
            return Color.Lerp(a, b, h);
        }

        public static float GetInnerRadius(float outerRadius)
        {
            return outerRadius*radiusRatio;
        }

        private static readonly Vector3[] corners = new Vector3[]
        {
            new Vector3(0f, 0f, 1f),
            new Vector3(radiusRatio, 0f, 0.5f),
            new Vector3(radiusRatio, 0f, -0.5f),
            new Vector3(0f, 0f, -1f),
            new Vector3(-radiusRatio, 0f, -0.5f),
            new Vector3(-radiusRatio, 0f, 0.5f),
            new Vector3(0f, 0f, 1f)
        };

        public static Vector3 GetRandomPosInTriangle(Vector3 a, Vector3 b, Vector3 c, float hashA, float hashB)
        {
            hashA = Mathf.Sqrt(hashA);

            return (1f - hashA) * a + (hashA * (1f - hashB)) * b + hashB * hashA * c;
        }

        public static Vector3 GetFirstSolidCorner(HexDirection direction, float solidFactor, float cellSize)
        {
            return corners[(int)direction] * solidFactor * cellSize;
        }

        public static Vector3 GetSecondSolidCorner(HexDirection direction, float solidFactor, float cellSize)
        {
            return corners[(int)direction + 1] * solidFactor * cellSize;
        }

        public static Vector3 GetFirstCorner(HexDirection direction, float cellSize)
        {
            return corners[(int)direction] * cellSize;
        }

        public static Vector3 GetSecondCorner(HexDirection direction, float cellSize)
        {
            return corners[(int)direction + 1] * cellSize;
        }

        public static Vector3 GetBridge(HexDirection direction, float blendFactor, float cellSize)
        {
            return (corners[(int)direction] + corners[(int)direction + 1]) * blendFactor * cellSize;
        }

        public static Vector3 Perturb(Vector3 position, float perturbSterngth)
        {
            Vector4 sample = HexUtils.SampleNoise(position);
            position.x += (sample.x * 2f - 1f) * perturbSterngth;
            position.z += (sample.z * 2f - 1f) * perturbSterngth;
            return position;
        }

        public static HexEdgeType GetEdgeType(int elevation1, int elevation2)
        {
            if (elevation1 == elevation2)
            {
                return HexEdgeType.Flat;
            }

            int delta = elevation2 - elevation1;
            if (Mathf.Abs(delta) <= 2)
            {
                return HexEdgeType.Slope;
            }

            return HexEdgeType.Cliff;
        }

        public static HexCoordinates CoordinatesFromPosition(Vector3 position, float cellSize)
        {
            float x = position.x / (HexUtils.GetInnerRadius(cellSize) * 2f);
            float y = -x;

            float offset = position.z / (cellSize * 3f);
            x -= offset;
            y -= offset;

            int iX = Mathf.RoundToInt(x);
            int iY = Mathf.RoundToInt(y);
            int iZ = Mathf.RoundToInt(-x - y);

            if (iX + iY + iZ != 0)
            {
                float dX = Mathf.Abs(x - iX);
                float dY = Mathf.Abs(y - iY);
                float dZ = Mathf.Abs(-x - y - iZ);

                if (dX > dY && dX > dZ)
                {
                    iX = -iY - iZ;
                }
                else if (dZ > dY)
                {
                    iZ = -iX - iY;
                }
            }

            return new HexCoordinates(iX, iZ);
        }

        public static HexDirection GetDirection(HexCoordinates start, HexCoordinates end)
        {
            Vector3 startVec = start.GetWorldCoordinates(1), endVec = end.GetWorldCoordinates(1);

            float angle = Vector3.Angle(endVec - startVec, Vector3.forward);

            bool east = endVec.x > startVec.x;

            if (angle > 120)
            {
                return east ? HexDirection.SE : HexDirection.SW;
            }
            else if (angle > 60)
            {
                return east ? HexDirection.E : HexDirection.W;
            }
            else
            {
                return east ? HexDirection.NE : HexDirection.NW;
            }
        }

        public static HexDirection GetDirection(HexCell start, HexCell end)
        {
            return GetDirection(start.coordinates, end.coordinates);
        }

        public const int hashGridSize = 256;

        static HexHash[] hashGrid;

        public static void InitializeHashGrid(int seed)
        {
            hashGrid = new HexHash[hashGridSize * hashGridSize];

            Random.State currentState = Random.state;
            Random.InitState(seed);
            for (int i = 0; i < hashGrid.Length; i++)
            {
                hashGrid[i] = HexHash.Create();
            }
            Random.state = currentState;
        }

        public static HexHash SampleHashGrid(Vector3 position)
        {
            int x = (int)(position.x * 0.25f) % hashGridSize;
            if (x < 0)
            {
                x += hashGridSize;
            }
            int z = (int)(position.z * 0.25f) % hashGridSize;
            if (z < 0)
            {
                z += hashGridSize;
            }
            return hashGrid[x + z * hashGridSize];
        }
    }

    public struct HexHash {

	public float a, b, c, d, e;

	public static HexHash Create () {
		HexHash hash;
		hash.a = Random.value * 0.999f;
		hash.b = Random.value * 0.999f;
        hash.c = Random.value * 0.999f;
        hash.d = Random.value * 0.999f;
        hash.e = Random.value * 0.999f;
        return hash;
	}
}
}
