﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

//for Texture2D

namespace Assets.Scripts.Game.TiledMapGenerator
{
    class BasicGenerator : IMapGenerator
    {
        private class Frontier
        {
            public HexCell cell;
            public int elevation;

            public Frontier(HexCell cell, int elevation)
            {
                this.cell = cell;
                this.elevation = elevation;
            }
        }

        private const float TEMPERATURE_JITTER = 1f;

        public BasicGenerator()
        {
        }

        private const int ELEVATION_MAX = 20;
        private const int SOURCES_OVERLAP = 1;

        private const float TEMPERATURE_BASE = 24.0f;
        private const float TEMPERATURE_HEIGHT_FACTOR = -0.1f;

        private Queue<Frontier> generationFrontier;

        public void GenerateMap(HexGrid map, int seed)
        {
            Random.InitState(seed);
            generationFrontier = new Queue<Frontier>();
            int sizX = map.SizeX, sizZ = map.SizeZ;

            int[,] generated = new int[map.SizeX, map.SizeZ];

            for (int i = 0; i < sizX; i++)
                for (int j = 0; j < sizZ; j++)
                    generated[i, j] = 0;

            int startingPointCount = map.SizeX * map.SizeZ / 50 + 1;
            for (int i = 0; i < startingPointCount; i++) {
                HexCoordinates startCoord = new HexCoordinates(Random.Range(0, sizX - 1), Random.Range(0, sizZ - 1));

                if(map.GetCell(startCoord) == null)//TODO: why is that neccessary?
                    continue;

                generationFrontier.Enqueue(new Frontier(map.GetCell(startCoord), (int) (ELEVATION_MAX * Random.value)));
            }

            while (generationFrontier.Count > 0) {
                Frontier frontier = generationFrontier.Dequeue();

                if (generated[frontier.cell.RawCoordinates.x, frontier.cell.RawCoordinates.y] >= SOURCES_OVERLAP)
                    continue;


                int frontierNeighbours = 3, heightSum = frontier.elevation * 3;
                for (int j = 0; j < 6; j++)
                {
                    HexCell neighbour = frontier.cell.GetNeighbor((HexDirection)j);

                    if (neighbour != null && generated[neighbour.RawCoordinates.x, neighbour.RawCoordinates.y] > 0)
                    {
                        frontierNeighbours++;
                        heightSum += neighbour.Elevation;
                    }
                }

                frontier.cell.Elevation = heightSum / frontierNeighbours;
                frontier.cell.TerrainType = HexGrid.GetTerrain(frontier.cell.Elevation, ELEVATION_MAX);
                generated[frontier.cell.RawCoordinates.x, frontier.cell.RawCoordinates.y]++;

                for (int i = 0; i < 6; i++) {
                    HexCell nextCell = frontier.cell.GetNeighbor((HexDirection) i);

                    if (nextCell == null || generated[nextCell.RawCoordinates.x, nextCell.RawCoordinates.y] >= SOURCES_OVERLAP)
                        continue;

                    int elevationMod = Mathf.RoundToInt(Random.value * 5) - 2;
                    generationFrontier.Enqueue(new Frontier(nextCell, frontier.elevation + elevationMod));
                    //generationFrontier.Enqueue(new Frontier(nextCell, frontier.cell.Elevation + elevationMod));
                }
            }
        }

        private static float CalculateRuggedness(HexCell cell, HexGrid map)
        {
            float ret = 0;

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                ret += cell.GetElevationDifference(d);
            }

            return ret;
        }
    }
}
