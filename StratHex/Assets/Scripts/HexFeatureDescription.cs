﻿using UnityEngine;

namespace Assets.Scripts {
    public class HexFeatureDescription : MonoBehaviour
    {
        public float Radius = 1f;
        public float YDisplacement = 0f;
    }
}
