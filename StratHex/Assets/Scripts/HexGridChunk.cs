﻿using UnityEngine;

namespace Assets.Scripts
{
    public class HexGridChunk : MonoBehaviour
    {
        HexGrid grid;
        HexCell[] cells;

        public HexFeatureManager Features;
        public HexMesh Terrain, Rivers, Roads, Water;
        Canvas gridCanvas;

        private float cellOuterRadius, cellInnerRadius;
        public float solidFactor = 0.75f;
        private float blendFactor;
        public float riverbedElevationOffset = -1f;
        public float waterSurfaceElevationOffset = -0.5f;

        void Awake()
        {
            grid = HexGrid.Instance;
            gridCanvas = GetComponentInChildren<Canvas>();

            cells = new HexCell[grid.chunkSizeX * grid.chunkSizeZ];
            blendFactor = 1f - solidFactor;
        }

        void Start()
        {
            Refresh();
        }

        void LateUpdate()
        {
            if (refreshNeeded)
            {
                refresh();
                refreshNeeded = false;
            }
        }

        public void AddCell(int index, HexCell cell)
        {
            cells[index] = cell;
            cell.chunk = this;
            cell.transform.SetParent(transform, false);
            cell.uiRect.SetParent(gridCanvas.transform, false);
        }

        private bool refreshNeeded = false;
        public void Refresh()
        {
            refreshNeeded = true;
        }

        private void refresh()
        {
            Triangulate(cells, grid.cellSize);
        }

        public void Triangulate(HexCell[] cells, float cellSize)
        {
            cellOuterRadius = cellSize;
            cellInnerRadius = HexUtils.GetInnerRadius(cellSize);

            Terrain.Clear();
            Rivers.Clear();
            Roads.Clear();
            Water.Clear();
            Features.Clear();

            for (int i = 0; i < cells.Length; i++)
            {
                // handles edge(incomplete) chunks
                if(cells[i] != null)
                    Triangulate(cells[i]);
            }

            Terrain.Apply();
            Rivers.Apply();
            Roads.Apply();
            Water.Apply();
            Features.Apply();
        }

        private void Triangulate(HexCell cell)
        {
            Vector3 center = cell.transform.localPosition;

            if (!cell.IsUnderwater)
            {
                Features.AddFeatures(cell);
            }

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                EdgeVertices edgeVertices = new EdgeVertices(center + HexUtils.GetFirstSolidCorner(d, solidFactor, cellOuterRadius), center + HexUtils.GetSecondSolidCorner(d, solidFactor, cellOuterRadius));

                if (cell.IsUnderwater)
                {
                    TriangulateWater(d, cell, center);
                }

                if (cell.HasRiver)
                {
                    if (cell.HasRiverThroughEdge(d))
                        edgeVertices.v[EdgeVertices.vertexCount / 2].y = cell.StreamBedY;

                    TriangulateSideWithRiver(d, cell, center, edgeVertices);
                }
                else
                {
                    TriangulateSideWithoutRiver(d, cell, center, edgeVertices);
                }

                if (d <= HexDirection.SE)
                {
                    TriangulateConnection(d, cell, edgeVertices);
                }
            }

            if (cell.HasRiver)
            {
                TriangulateCenterWithRivers(cell);
                TriangulateRiverSurface(cell);
            }
        }

        void TriangulateWater(HexDirection direction, HexCell cell, Vector3 center)
        {
            center.y = cell.WaterSurfaceY;
            Vector3 c1 = center + HexUtils.GetFirstCorner(direction, cellOuterRadius);
            Vector3 c2 = center + HexUtils.GetSecondCorner(direction, cellOuterRadius);

            Water.AddTriangle(center, c1, c2);
            Water.AddTriangleColor(HexUtils.RiverColor);

            if (direction <= HexDirection.SE)
            {
                HexCell neighbor = cell.GetNeighbor(direction);
                if (neighbor == null || !neighbor.IsUnderwater)
                {
                    return;
                }

                Vector3 bridge = HexUtils.GetBridge(direction, blendFactor, cellOuterRadius);
                Vector3 e1 = c1 + bridge;
                Vector3 e2 = c2 + bridge;

                Water.AddQuad(c1, c2, e1, e2);
                Water.AddQuadColor(HexUtils.RiverColor);

                if (direction <= HexDirection.E)
                {
                    HexCell nextNeighbor = cell.GetNeighbor(direction.Next());
                    if (nextNeighbor == null || !nextNeighbor.IsUnderwater)
                    {
                        return;
                    }
                    Water.AddTriangle(c2, e2, c2 + HexUtils.GetBridge(direction.Next(), blendFactor, cellOuterRadius));
                    Water.AddTriangleColor(HexUtils.RiverColor);
                }
            }
        }

        void TriangulateSideWithoutRiver(HexDirection direction, HexCell cell, Vector3 center, EdgeVertices e)
        {
            TriangulateEdgeFan(center, e, cell.Color);

            if (cell.HasRoads)
            {
                Vector2 interpolators = GetRoadInterpolators(direction, cell);
                TriangulateRoad(center, Vector3.Lerp(center, e.v[0], interpolators.x), Vector3.Lerp(center, e.v[EdgeVertices.vertexCount-1], interpolators.y), e, cell.Roads[(int)direction]);
            }
        }

        private void TriangulateRiverSurface(HexCell cell)
        {
            Vector3 center = cell.transform.localPosition;
            center.y = cell.RiverSurfaceY;

            for (HexDirection d = HexDirection.NE; d <= HexDirection.NW; d++)
            {
                Rivers.AddTriangle(center, center + HexUtils.GetFirstSolidCorner(d, solidFactor, cellOuterRadius), center + HexUtils.GetSecondSolidCorner(d, solidFactor, cellOuterRadius));
                Rivers.AddTriangleColor(HexUtils.RiverColor);
            }
        }

        private void TriangulateCenterWithRivers(HexCell cell)
        {
            HexDirection direction = HexDirection.E;
            while (!cell.HasRiverThroughEdge(direction))
            {
                direction = direction.Next();
            }

            HexDirection prevDirection = direction;

            for (int i = 0; i <= 6; i++)
            {
                direction = direction.Next();

                if (!cell.HasRiverThroughEdge(direction))
                    continue;

                TriangulateCenterSector(cell, prevDirection, direction);

                prevDirection = direction;
            }

            for (int i = 0; i <= 6; i++) {
                direction = direction.Next();
                if (cell.HasRoadThroughEdge(direction)) {
                    Vector3 center = cell.transform.position;
                    Vector3 srcOffset = direction.UnitVector() * solidFactor * cellInnerRadius * 0.50f;
                    Vector3 dstOffset = -1.0f * srcOffset;

                    if (cell.HasRoadThroughEdge(direction.Opposite())) {
                        Features.AddBridge(center + srcOffset, center + dstOffset);
                    } else if (cell.HasRoadThroughEdge(direction.Opposite().Previous())) {
                        dstOffset = Quaternion.AngleAxis(-60, Vector3.up) * dstOffset;
                        Features.AddBridge(center + srcOffset, center + dstOffset);
                    } else if (cell.HasRoadThroughEdge(direction.Opposite().Next())) {
                        dstOffset = Quaternion.AngleAxis(60, Vector3.up) * dstOffset;
                        Features.AddBridge(center + srcOffset, center + dstOffset);
                    }

                    break;
                }
            }
        }

        void TriangulateRoadSegment(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4, Vector3 v5, Vector3 v6)
        {
            Roads.AddQuad(v1, v2, v4, v5);
            Roads.AddQuad(v2, v3, v5, v6);
            Roads.AddQuadUV(0f, 1f, 0f, 0f);
            Roads.AddQuadUV(1f, 0f, 0f, 0f);
        }

        void TriangulateRoad(Vector3 center, Vector3 mL, Vector3 mR, EdgeVertices e, RoadType roadType)
        {
            if (roadType != RoadType.None)
            {
                Vector3 mC = Vector3.Lerp(mL, mR, 0.5f);
                TriangulateRoadSegment(mL, mC, mR, e.v[1], e.v[2], e.v[3]);

                Roads.AddTriangle(center, mL, mC);
                Roads.AddTriangle(center, mC, mR);
                Roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(1f, 0f));
                Roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(1f, 0f), new Vector2(0f, 0f));
            }
            else
            {
                TriangulateRoadEdge(center, mL, mR);
            }
        }

        void TriangulateRoadEdge(Vector3 center, Vector3 mL, Vector3 mR)
        {
            Roads.AddTriangle(center, mL, mR);
            Roads.AddTriangleUV(new Vector2(1f, 0f), new Vector2(0f, 0f), new Vector2(0f, 0f));
        }

        Vector2 GetRoadInterpolators(HexDirection direction, HexCell cell)
        {
            Vector2 interpolators;
            if (cell.HasRoadThroughEdge(direction))
            {
                interpolators.x = interpolators.y = 0.5f;
            }
            else
            {
                interpolators.x = cell.HasRoadThroughEdge(direction.Previous()) ? 0.5f : 0.25f;
                interpolators.y = cell.HasRoadThroughEdge(direction.Next()) ? 0.5f : 0.25f;
            }
            return interpolators;
        }

        public bool IsCoordOverRoad(Vector2 coord, HexCell cell)
        {


            return false;
        }

        private void TriangulateCenterSector(HexCell cell, HexDirection prevDirection, HexDirection direction)
        {
            Vector3 center = cell.transform.localPosition;
            Vector3 centerRiverbed = cell.transform.localPosition;
            centerRiverbed.y = cell.StreamBedY;

            Vector3[] midpoints = new Vector3[6];

            for (int i = 0; i < midpoints.Length; i++)
            {
                midpoints[i] = center + HexUtils.GetFirstSolidCorner((HexDirection)i, solidFactor, cellOuterRadius) * 0.50f;
            }

            int directionDifference = direction - prevDirection;
            if (directionDifference < 0)
                directionDifference += 6;

            Vector3 leftRiverbed = GetRiverbedPoint(midpoints[(int)prevDirection], midpoints[(int)prevDirection.Next()], cell);
            Vector3 rightRiverbed = GetRiverbedPoint(midpoints[(int)direction], midpoints[(int)direction.Next()], cell);
            Vector3 midMidpoint;

            switch (directionDifference)
            {
                case 0://360 - river ending
                    Terrain.AddTriangle(midpoints[(int)direction], GetRiverbedPoint(midpoints[(int)direction], midpoints[(int)direction.Next()], cell), center);
                    Terrain.AddTriangleColor(cell.Color);

                    Terrain.AddTriangle(GetRiverbedPoint(midpoints[(int)direction], midpoints[(int)direction.Next()], cell), midpoints[(int)direction.Next()], center);
                    Terrain.AddTriangleColor(cell.Color);

                    direction = direction.Next();
                    do
                    {
                        midMidpoint = Vector3.Lerp(midpoints[(int)direction], midpoints[(int)direction.Next()], 0.5f);
                        Terrain.AddTriangle(midpoints[(int)direction], midMidpoint, center);
                        Terrain.AddTriangleColor(cell.Color);
                        Terrain.AddTriangle(midMidpoint, midpoints[(int)direction.Next()], center);
                        Terrain.AddTriangleColor(cell.Color);

                        direction = direction.Next();
                    } while (direction != prevDirection);

                    break;
                case 1:
                    Terrain.AddTriangle(leftRiverbed, midpoints[(int)direction], rightRiverbed);
                    Terrain.AddTriangleColor(cell.Color);

                    Terrain.AddTriangle(leftRiverbed, rightRiverbed, centerRiverbed);
                    Terrain.AddTriangleColor(cell.Color);
                    break;
                case 2:
                    midMidpoint = Vector3.Lerp(midpoints[(int)prevDirection.Next()], midpoints[(int)direction], 0.5f);

                    Terrain.AddTriangle(leftRiverbed, midpoints[(int)prevDirection.Next()], midMidpoint);
                    Terrain.AddTriangleColor(cell.Color);
                    Terrain.AddTriangle(midMidpoint, rightRiverbed, leftRiverbed);
                    Terrain.AddTriangleColor(cell.Color);
                    Terrain.AddTriangle(midpoints[(int)direction], rightRiverbed, midMidpoint);
                    Terrain.AddTriangleColor(cell.Color);

                    Terrain.AddTriangle(leftRiverbed, rightRiverbed, centerRiverbed);
                    Terrain.AddTriangleColor(cell.Color);
                    break;
                case 3:
                    Terrain.AddTriangle(leftRiverbed, midpoints[(int)prevDirection.Next()], centerRiverbed);
                    Terrain.AddTriangleColor(cell.Color);
                    Terrain.AddTriangle(centerRiverbed, midpoints[(int)prevDirection.Next()], midpoints[(int)direction]);
                    Terrain.AddTriangleColor(cell.Color);
                    Terrain.AddTriangle(midpoints[(int)direction], rightRiverbed, centerRiverbed);
                    Terrain.AddTriangleColor(cell.Color);

                    Vector3 midMidpointL = Vector3.Lerp(midpoints[(int)direction], midpoints[(int)direction.Previous()], 0.5f);
                    Vector3 midMidpointR = Vector3.Lerp(midpoints[(int)direction.Previous()], midpoints[(int)direction.Previous().Previous()], 0.5f);

                    Terrain.AddTriangle(midpoints[(int)direction.Previous()], midMidpointL, midMidpointR);
                    Terrain.AddTriangleColor(cell.Color);

                    Terrain.AddQuad(midpoints[(int)prevDirection.Next()], midpoints[(int)direction], midMidpointR, midMidpointL);
                    Terrain.AddQuadColor(cell.Color);
                    break;
                case 4:
                    Terrain.AddTriangle(leftRiverbed, midpoints[(int)prevDirection.Next()], center);
                    Terrain.AddTriangleColor(cell.Color);
                    Terrain.AddTriangle(center, rightRiverbed, leftRiverbed);
                    Terrain.AddTriangleColor(cell.Color);
                    Terrain.AddTriangle(midpoints[(int)direction], rightRiverbed, center);
                    Terrain.AddTriangleColor(cell.Color);

                    direction = direction.Previous();
                    do
                    {
                        midMidpoint = Vector3.Lerp(midpoints[(int)direction], midpoints[(int)direction.Next()], 0.5f);
                        Terrain.AddTriangle(midpoints[(int)direction], midMidpoint, center);
                        Terrain.AddTriangleColor(cell.Color);
                        Terrain.AddTriangle(midMidpoint, midpoints[(int)direction.Next()], center);
                        Terrain.AddTriangleColor(cell.Color);

                        direction = direction.Previous();
                    } while (direction != prevDirection);
                    break;
                case 5:
                    Vector3 alteredCenter = center + HexUtils.GetFirstSolidCorner(direction.Previous().Previous(), solidFactor, cellOuterRadius) * 0.25f;

                    Terrain.AddTriangle(rightRiverbed, centerRiverbed, midpoints[(int)direction]);
                    Terrain.AddTriangleColor(cell.Color);
                    Terrain.AddTriangle(alteredCenter, midpoints[(int)direction], centerRiverbed);
                    Terrain.AddTriangleColor(cell.Color);

                    direction = direction.Previous();
                    do
                    {
                        midMidpoint = Vector3.Lerp(midpoints[(int)direction], midpoints[(int)direction.Next()], 0.5f);
                        Terrain.AddTriangle(midpoints[(int)direction], midMidpoint, alteredCenter);
                        Terrain.AddTriangleColor(cell.Color);
                        Terrain.AddTriangle(midMidpoint, midpoints[(int)direction.Next()], alteredCenter);
                        Terrain.AddTriangleColor(cell.Color);

                        direction = direction.Previous();
                    } while (direction != prevDirection);

                    Terrain.AddTriangle(midpoints[(int)prevDirection.Next()], centerRiverbed, leftRiverbed);
                    Terrain.AddTriangleColor(cell.Color);
                    Terrain.AddTriangle(centerRiverbed, midpoints[(int)prevDirection.Next()], alteredCenter);
                    Terrain.AddTriangleColor(cell.Color);

                    break;
            }
        }

        private Vector3 GetRiverbedPoint(Vector3 leftMidpoint, Vector3 rightMidpoint, HexCell cell)
        {
            Vector3 result = Vector3.Lerp(leftMidpoint, rightMidpoint, 0.5f);
            result.y = cell.StreamBedY;
            return result;
        }

        void TriangulateSideWithRiver(HexDirection direction, HexCell cell, Vector3 center, EdgeVertices e)
        {
            Vector3 centerL = center + HexUtils.GetFirstSolidCorner(direction.Previous(), solidFactor, cellOuterRadius) * 0.25f;

            Vector3 centerR = center + HexUtils.GetSecondSolidCorner(direction.Next(), solidFactor, cellOuterRadius) * 0.25f;

            EdgeVertices m = new EdgeVertices(Vector3.Lerp(centerL, e.v[0], 0.5f), Vector3.Lerp(centerR, e.v[EdgeVertices.vertexCount - 1], 0.5f), 1f / 6f);

            m.v[EdgeVertices.vertexCount / 2].y = e.v[EdgeVertices.vertexCount / 2].y;

            Terrain.AddTriangle(m.v[1], e.v[0], e.v[1]);
            Terrain.AddTriangleColor(cell.Color);
            Terrain.AddTriangle(m.v[EdgeVertices.vertexCount - 2], e.v[EdgeVertices.vertexCount - 2], e.v[EdgeVertices.vertexCount - 1]);
            Terrain.AddTriangleColor(cell.Color);

            for (int i = 1; i < EdgeVertices.vertexCount - 2; i++)
            {
                Terrain.AddQuad(m.v[i], m.v[i + 1], e.v[i], e.v[i + 1]);
                Terrain.AddQuadColor(cell.Color);
            }

            if (cell.HasRoadThroughEdge(direction))
            {
                TriangulateRoadSegment(m.v[1], m.v[2], m.v[3], e.v[1], e.v[2], e.v[3]);
            }

            /*if (!cell.IsUnderwater && !cell.HasRoadThroughEdge(direction) && !cell.HasRiverThroughEdge(direction))
            {
                Features.AddFeature(cell, (center + e.v[0] + e.v[EdgeVertices.vertexCount - 1])*(1f/3f));
            }*/
        }

        void TriangulateEdgeFan(Vector3 center, EdgeVertices edge, Color color)
        {
            for (int i = 0; i < EdgeVertices.vertexCount - 1; i++)
            {
                Terrain.AddTriangle(center, edge.v[i], edge.v[i + 1]);
                Terrain.AddTriangleColor(color);
            }
        }

        void TriangulateEdgeStrip(EdgeVertices e1, Color c1, EdgeVertices e2, Color c2, RoadType road)
        {
            for (int i = 0; i < EdgeVertices.vertexCount - 1; i++)
            {
                Terrain.AddQuad(e1.v[i], e1.v[i + 1], e2.v[i], e2.v[i + 1]);
                Terrain.AddQuadColor(c1, c2);
            }

            if (road != RoadType.None)
            {
                TriangulateRoadSegment(e1.v[1], e1.v[2], e1.v[3], e2.v[1], e2.v[2], e2.v[3]);
            }
        }

        void TriangulateConnection(HexDirection direction, HexCell cell, EdgeVertices e1)
        {
            HexCell neighbor = cell.GetNeighbor(direction);

            if (neighbor == null)
                return;

            Vector3 bridge = HexUtils.GetBridge(direction, blendFactor, cellOuterRadius);
            bridge.y = neighbor.Position.y - cell.Position.y;
            EdgeVertices e2 = new EdgeVertices(e1.v[0] + bridge, e1.v[EdgeVertices.vertexCount - 1] + bridge);

            if (cell.HasRiverThroughEdge(direction))
            {
                e2.v[EdgeVertices.vertexCount / 2].y = neighbor.StreamBedY;

                Vector3 a = e1.v[0], b = e1.v[EdgeVertices.vertexCount - 1], c = e2.v[0], d = e2.v[EdgeVertices.vertexCount - 1];
                a.y = cell.RiverSurfaceY;
                b.y = cell.RiverSurfaceY;
                c.y = neighbor.RiverSurfaceY;
                d.y = neighbor.RiverSurfaceY;

                Rivers.AddQuad(a, b, c, d);
                Rivers.AddQuadColor(HexUtils.RiverColor);
            }

            if (cell.GetEdgeType(direction) == HexEdgeType.Slope)
            {
                TriangulateEdgeTerraces(e1, cell, e2, neighbor, cell.Roads[(int)direction]);
            }
            else
            {
                TriangulateEdgeStrip(e1, cell.Color, e2, neighbor.Color, cell.Roads[(int)direction]);
            }


            HexCell nextNeighbor = cell.GetNeighbor(direction.Next());
            if (direction <= HexDirection.E && nextNeighbor != null)
            {
                Vector3 v5 = e1.v[EdgeVertices.vertexCount - 1] + HexUtils.GetBridge(direction.Next(), blendFactor, cellOuterRadius);
                v5.y = nextNeighbor.Elevation * cell.Grid.elevationStep;

                if (cell.Elevation <= neighbor.Elevation)
                {
                    if (cell.Elevation <= nextNeighbor.Elevation)
                    {
                        TriangulateCorner(e1.v[EdgeVertices.vertexCount - 1], cell, e2.v[EdgeVertices.vertexCount - 1], neighbor, v5, nextNeighbor);
                    }
                    else
                    {
                        TriangulateCorner(v5, nextNeighbor, e1.v[EdgeVertices.vertexCount - 1], cell, e2.v[EdgeVertices.vertexCount - 1], neighbor);
                    }
                }
                else if (neighbor.Elevation <= nextNeighbor.Elevation)
                {
                    TriangulateCorner(e2.v[EdgeVertices.vertexCount - 1], neighbor, v5, nextNeighbor, e1.v[EdgeVertices.vertexCount - 1], cell);
                }
                else
                {
                    TriangulateCorner(v5, nextNeighbor, e1.v[EdgeVertices.vertexCount - 1], cell, e2.v[EdgeVertices.vertexCount - 1], neighbor);
                }
            }
        }

        void TriangulateEdgeTerraces(EdgeVertices begin, HexCell beginCell, EdgeVertices end, HexCell endCell, RoadType road)
        {
            EdgeVertices e2 = HexUtils.TerraceLerp(begin, end, 1);
            Color c2 = HexUtils.TerraceLerp(beginCell.Color, endCell.Color, 1);

            TriangulateEdgeStrip(begin, beginCell.Color, e2, c2, road);

            for (int i = 2; i < HexUtils.terraceSteps; i++)
            {
                EdgeVertices e1 = e2;
                Color c1 = c2;

                e2 = HexUtils.TerraceLerp(begin, end, i);
                c2 = HexUtils.TerraceLerp(beginCell.Color, endCell.Color, i);

                TriangulateEdgeStrip(e1, c1, e2, c2, road);
            }

            TriangulateEdgeStrip(e2, c2, end, endCell.Color, road);
        }

        void TriangulateCorner(Vector3 bottom, HexCell bottomCell, Vector3 left, HexCell leftCell, Vector3 right, HexCell rightCell)
        {
            HexEdgeType leftEdgeType = bottomCell.GetEdgeType(leftCell);
            HexEdgeType rightEdgeType = bottomCell.GetEdgeType(rightCell);

            if (leftEdgeType == HexEdgeType.Slope)
            {
                if (rightEdgeType == HexEdgeType.Slope)
                {
                    if (leftCell.GetEdgeType(rightCell) == HexEdgeType.Slope)
                        TriangulateCornerThreeTeraces(bottom, bottomCell, left, leftCell, right, rightCell);
                    else
                        TriangulateCornerTerraces(bottom, bottomCell, left, leftCell, right, rightCell);
                }
                else if (rightEdgeType == HexEdgeType.Flat)
                    TriangulateCornerTerraces(left, leftCell, right, rightCell, bottom, bottomCell);
                else
                    TriangulateCornerTerracesCliff(bottom, bottomCell, left, leftCell, right, rightCell);
            }
            else if (rightEdgeType == HexEdgeType.Slope)
            {
                if (leftEdgeType == HexEdgeType.Flat)
                    TriangulateCornerTerraces(right, rightCell, bottom, bottomCell, left, leftCell);
                else
                    TriangulateCornerCliffTerraces(bottom, bottomCell, left, leftCell, right, rightCell);
            }
            else if (leftCell.GetEdgeType(rightCell) == HexEdgeType.Slope)
            {
                if (leftCell.Elevation < rightCell.Elevation)
                    TriangulateCornerCliffTerraces(right, rightCell, bottom, bottomCell, left, leftCell);
                else
                    TriangulateCornerTerracesCliff(left, leftCell, right, rightCell, bottom, bottomCell);
            }
            else
            {
                Terrain.AddTriangle(bottom, left, right);
                Terrain.AddTriangleColor(bottomCell.Color, leftCell.Color, rightCell.Color);
            }
        }

        void TriangulateCornerTerraces(Vector3 begin, HexCell beginCell, Vector3 left, HexCell leftCell, Vector3 right, HexCell rightCell)
        {
            Vector3 v3 = HexUtils.TerraceLerp(begin, left, 1);
            Vector3 v4 = HexUtils.TerraceLerp(begin, right, 1);
            Color c3 = HexUtils.TerraceLerp(beginCell.Color, leftCell.Color, 1);
            Color c4 = HexUtils.TerraceLerp(beginCell.Color, rightCell.Color, 1);

            Terrain.AddTriangle(begin, v3, v4);
            Terrain.AddTriangleColor(beginCell.Color, c3, c4);

            for (int i = 2; i < HexUtils.terraceSteps; i++)
            {
                Vector3 v1 = v3;
                Vector3 v2 = v4;
                Color c1 = c3;
                Color c2 = c4;
                v3 = HexUtils.TerraceLerp(begin, left, i);
                v4 = HexUtils.TerraceLerp(begin, right, i);
                c3 = HexUtils.TerraceLerp(beginCell.Color, leftCell.Color, i);
                c4 = HexUtils.TerraceLerp(beginCell.Color, rightCell.Color, i);
                Terrain.AddQuad(v1, v2, v3, v4);
                Terrain.AddQuadColor(c1, c2, c3, c4);
            }

            Terrain.AddQuad(v3, v4, left, right);
            Terrain.AddQuadColor(c3, c4, leftCell.Color, rightCell.Color);
        }

        void TriangulateCornerTerracesCliff(Vector3 begin, HexCell beginCell, Vector3 left, HexCell leftCell, Vector3 right, HexCell rightCell)
        {
            float b = 1f / (rightCell.Elevation - beginCell.Elevation);
            if (b < 0)
            {
                b = -b;
            }

            Vector3 boundary = Vector3.Lerp(HexUtils.Perturb(begin, HexUtils.perturbSterngth), HexUtils.Perturb(right, HexUtils.perturbSterngth), b);
            Color boundaryColor = Color.Lerp(beginCell.Color, rightCell.Color, b);

            TriangulateBoundaryTriangle(begin, beginCell, left, leftCell, boundary, boundaryColor);

            if (leftCell.GetEdgeType(rightCell) == HexEdgeType.Slope)
            {
                TriangulateBoundaryTriangle(left, leftCell, right, rightCell, boundary, boundaryColor);
            }
            else
            {
                Terrain.AddTriangle(HexUtils.Perturb(left, HexUtils.perturbSterngth), HexUtils.Perturb(right, HexUtils.perturbSterngth), boundary, false);
                Terrain.AddTriangleColor(leftCell.Color, rightCell.Color, boundaryColor);
            }
        }

        void TriangulateCornerCliffTerraces(Vector3 begin, HexCell beginCell, Vector3 left, HexCell leftCell, Vector3 right, HexCell rightCell)
        {
            float b = 1f / (leftCell.Elevation - beginCell.Elevation);
            if (b < 0)
            {
                b = -b;
            }

            Vector3 boundary = Vector3.Lerp(HexUtils.Perturb(begin, HexUtils.perturbSterngth), HexUtils.Perturb(left, HexUtils.perturbSterngth), b);
            Color boundaryColor = Color.Lerp(beginCell.Color, leftCell.Color, b);

            TriangulateBoundaryTriangle(right, rightCell, begin, beginCell, boundary, boundaryColor);

            if (leftCell.GetEdgeType(rightCell) == HexEdgeType.Slope)
            {
                TriangulateBoundaryTriangle(left, leftCell, right, rightCell, boundary, boundaryColor);
            }
            else
            {
                Terrain.AddTriangle(HexUtils.Perturb(left, HexUtils.perturbSterngth), HexUtils.Perturb(right, HexUtils.perturbSterngth), boundary, false);
                Terrain.AddTriangleColor(leftCell.Color, rightCell.Color, boundaryColor);
            }
        }

        void TriangulateCornerThreeTeraces(Vector3 begin, HexCell beginCell, Vector3 left, HexCell leftCell, Vector3 right, HexCell rightCell)
        {
            Vector3 v1, v2;
            Vector3 v3 = HexUtils.TerraceLerp(begin, left, 1);
            Vector3 v4 = HexUtils.TerraceLerp(begin, right, 1);
            Color c1, c2;
            Color c3 = HexUtils.TerraceLerp(beginCell.Color, leftCell.Color, 1);
            Color c4 = HexUtils.TerraceLerp(beginCell.Color, rightCell.Color, 1);

            Terrain.AddTriangle(begin, v3, v4);
            Terrain.AddTriangleColor(beginCell.Color, c3, c4);

            for (int i = 2; i < HexUtils.terraceSteps; i++) {
                v1 = v3;
                v2 = v4;
                c1 = c3;
                c2 = c4;
                v3 = HexUtils.TerraceLerp(begin, left, i);
                v4 = HexUtils.TerraceLerp(begin, right, i);
                c3 = HexUtils.TerraceLerp(beginCell.Color, leftCell.Color, i);
                c4 = HexUtils.TerraceLerp(beginCell.Color, rightCell.Color, i);
                Terrain.AddQuad(v1, v2, v3, v4);
                Terrain.AddQuadColor(c1, c2, c3, c4);
            }

            // stick to the terrace between left and right cells

            // triangles to fill terrace protrusions from the side
            Vector3 lower, higher, beginToLowerTerrace;
            HexCell lowerCell, higherCell;
            Color beginToLowerTerraceColor;

            if (rightCell.Elevation > leftCell.Elevation) {
                lower = left; higher = right;
                lowerCell = leftCell; higherCell = rightCell;
            } else {
                lower = right; higher = left;
                lowerCell = rightCell; higherCell = leftCell;
            }

            beginToLowerTerrace = HexUtils.TerraceLerp(begin, lower, HexUtils.terraceSteps - 1);
            beginToLowerTerraceColor = HexUtils.TerraceLerp(beginCell.Color, lowerCell.Color, HexUtils.terraceSteps - 1);

            v3 = lower;
            c3 = lowerCell.Color;
            for (int i = 2; i < HexUtils.terraceSteps; i+=2) {
                v1 = v3;
                v2 = HexUtils.TerraceLerp(lower, higher, i - 1);
                v3 = HexUtils.TerraceLerp(lower, higher, i);
                c1 = c3;
                c2 = HexUtils.TerraceLerp(lowerCell.Color, higherCell.Color, i - 1);
                c3 = HexUtils.TerraceLerp(lowerCell.Color, higherCell.Color, i);

                if (lower == left) {
                    Terrain.AddTriangle(v1, v2, v3);
                    Terrain.AddTriangleColor(c1, c2, c3);

                    Terrain.AddTriangle(v1, v3, beginToLowerTerrace);
                    Terrain.AddTriangleColor(c1, c3, beginToLowerTerraceColor);
                } else {
                    Terrain.AddTriangle(v1, v3, v2);
                    Terrain.AddTriangleColor(c1, c3, c2);

                    Terrain.AddTriangle(v3, v1, beginToLowerTerrace);
                    Terrain.AddTriangleColor(c3, c1, beginToLowerTerraceColor);
                }
            }

            v1 = v3;
            v2 = higher;
            v3 = HexUtils.TerraceLerp(begin, higher, HexUtils.terraceSteps - 1);
            c1 = c3;
            c2 = higherCell.Color;
            c3 = HexUtils.TerraceLerp(beginCell.Color, higherCell.Color, HexUtils.terraceSteps - 1);

            if (lower == left) {
                Terrain.AddTriangle(v1, v2, v3);
                Terrain.AddTriangleColor(c1, c2, c3);

                Terrain.AddTriangle(v1, v3, beginToLowerTerrace);
                Terrain.AddTriangleColor(c1, c3, beginToLowerTerraceColor);
            } else {
                Terrain.AddTriangle(v1, v3, v2);
                Terrain.AddTriangleColor(c1, c3, c2);

                Terrain.AddTriangle(v3, v1, beginToLowerTerrace);
                Terrain.AddTriangleColor(c3, c1, beginToLowerTerraceColor);
            }
        }

        void TriangulateBoundaryTriangle(Vector3 begin, HexCell beginCell, Vector3 left, HexCell leftCell, Vector3 boundary, Color boundaryColor)
        {
            Vector3 v2 = HexUtils.Perturb(HexUtils.TerraceLerp(begin, left, 1), HexUtils.perturbSterngth);
            Color c2 = HexUtils.TerraceLerp(beginCell.Color, leftCell.Color, 1);

            Terrain.AddTriangle(HexUtils.Perturb(begin, HexUtils.perturbSterngth), v2, boundary, false);
            Terrain.AddTriangleColor(beginCell.Color, c2, boundaryColor);

            for (int i = 2; i < HexUtils.terraceSteps; i++)
            {
                Vector3 v1 = v2;
                Color c1 = c2;
                v2 = HexUtils.Perturb(HexUtils.TerraceLerp(begin, left, i), HexUtils.perturbSterngth);
                c2 = HexUtils.TerraceLerp(beginCell.Color, leftCell.Color, i);
                Terrain.AddTriangle(v1, v2, boundary, false);
                Terrain.AddTriangleColor(c1, c2, boundaryColor);
            }

            Terrain.AddTriangle(v2, HexUtils.Perturb(left, HexUtils.perturbSterngth), boundary, false);
            Terrain.AddTriangleColor(c2, leftCell.Color, boundaryColor);
        }
    }
}
