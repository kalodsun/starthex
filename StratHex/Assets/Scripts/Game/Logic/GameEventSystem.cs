﻿
using Assets.Scripts.Game.Entities;

namespace Assets.Scripts.Game.Logic
{
    public static class GameEventSystem
    {
        //public enum UnitEventType { Created, GivenOrders, Moved, Damaged, Destroyed }
        //public enum TileEventType { Changed, UnitEntered, UnitLeft }
        public enum GameEventType { TurnProcessed, GameReloaded }

        //public delegate void UnitEventDelegate(UnitEventType eventType, Unit unit, Unit extraUnit = null);
        //public delegate void TileEventDelegate(TileEventType eventType, HexCell cell, Unit unit = null);
        public delegate void GameEventDelegate(GameEventType eventType);

        //public static event UnitEventDelegate UnitEvent;
        //public static event TileEventDelegate TileEvent;
        public static event GameEventDelegate GameEvent;

        /*public static void SendUnitEvent(UnitEventType eventType, Unit unit, Unit extraUnit = null)
        {
            if(UnitEvent != null)
                UnitEvent.Invoke(eventType, unit, extraUnit);
        }

        public static void SendTileEvent(TileEventType eventType, HexCell cell, Unit unit = null)
        {
            if (TileEvent != null)
                TileEvent.Invoke(eventType, cell, unit);
        }*/

        public static void SendGameEvent(GameEventType eventType)
        {
            if (GameEvent != null)
                GameEvent.Invoke(eventType);
        }

    }
}
