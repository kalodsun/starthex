﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Game.Entities;
using UnityEngine;

namespace Assets.Scripts.Game.Logic
{
    /*public class TurnHistory
    {
        public class BattleRoundHistory
        {
            public Dictionary<Unit, Dictionary<Unit.StatValues, float>> UnitValues = new Dictionary<Unit, Dictionary<Unit.StatValues, float>>();
            public Dictionary<Unit, UnitAction.ActionType> UnitActions = new Dictionary<Unit, UnitAction.ActionType>();
            public float AttackerSuccess, DefenderSuccess;

            public int GetValueFloor(Unit.StatValues valueType, Unit unit)
            {
                if (UnitValues.ContainsKey(unit)) {
                    return Mathf.FloorToInt(UnitValues[unit][valueType]);
                }

                return -1;
            }
        }

        public class BattleHistory
        {
            public HexCell BattleCell { get; set; }

            public HashSet<Unit> Attackers { get; set; }
            public HashSet<Unit> Defenders { get; set; }
            public List<BattleRoundHistory> Rounds { get; set; }

            public Dictionary<Unit, Dictionary<Unit.StatValues, float>> StartValues = new Dictionary<Unit, Dictionary<Unit.StatValues, float>>();

            public bool BattleOver;
            public bool DefenderVictorious;

            public int GetCasualtiesForRoundFloor(Unit.StatValues valueType, Unit unit, int round)
            {
                float prevVal;
                prevVal = (round == 0) ? StartValues[unit][valueType] : Rounds[round - 1].UnitValues[unit][valueType];

                return Mathf.FloorToInt(prevVal - Rounds[round].UnitValues[unit][valueType]);
            }

            public int GetCasualtiesFloor(Unit unit, Unit.StatValues valueType = Unit.StatValues.Strength)
            {
                return Mathf.FloorToInt(GetCasualties(unit, valueType));
            }

            public float GetCasualties(Unit unit, Unit.StatValues valueType = Unit.StatValues.Strength)
            {
                return StartValues[unit][valueType] - Rounds.Last().UnitValues[unit][valueType];
            }

            public BattleHistory()
            {
                Rounds = new List<BattleRoundHistory>();
                Attackers = new HashSet<Unit>();
            }
        }

        public int TurnNumber { get; set; }

        public List<UnitAction> CompletedActions { get; set; }
        public List<BattleHistory> BattleHistories { get; set; }

        public TurnHistory()
        {
            CompletedActions = new List<UnitAction>();
            BattleHistories = new List<BattleHistory>();
        }
    }*/
}
