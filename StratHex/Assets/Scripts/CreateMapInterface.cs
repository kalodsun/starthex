﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class CreateMapInterface : MonoBehaviour
    {
        public UIScript UiScript;
        public Button CancelButton, CreateButton;
        public InputField XChunksInput, YChunksInput;
        public Toggle GenerateMapToggle, HeightmapToggle;
        public Dropdown HeightmapDropdown;

        public Texture2D[] Heightmaps;

        // Use this for initialization
        void Start () {
		    CancelButton.onClick.AddListener(() => {
                gameObject.SetActive(false);
		    });

            foreach (Texture2D heightmap in Heightmaps) {
                List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();

                options.Add(new Dropdown.OptionData(heightmap.name + " " + heightmap.width + "x" + heightmap.height));

                HeightmapDropdown.AddOptions(options);
            }

            HeightmapDropdown.onValueChanged.AddListener(selectedIndex => { HeightmapToggle.isOn = true; });

            CreateButton.onClick.AddListener(CreateMap);
        }

        public void CreateMap()
        {
            Texture2D heightmap = null;
            if (HeightmapToggle.isOn)
                heightmap = Heightmaps[HeightmapDropdown.value];

            UiScript.CreateMap(int.Parse(XChunksInput.text), int.Parse(YChunksInput.text), GenerateMapToggle.isOn, heightmap);
        }
    }
}
