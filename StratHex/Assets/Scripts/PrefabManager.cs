﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class PrefabManager
    {
        public static PrefabManager Instance { get; private set; }


        private const string STRUCTURE_PREFAB_DIR = "Prefabs/Structures/";
        private const string UNIT_PREFAB_DIR = "Prefabs/Units/";

        private readonly Dictionary<string, GameObject> prototypePrefabs = new Dictionary<string, GameObject>();

        private readonly Dictionary<string, LinkedList<GameObject>> prefabPool = new Dictionary<string, LinkedList<GameObject>>();

        public PrefabManager()
        {
            Assert.IsNull(Instance);
            Instance = this;
        }

        public void ReturnPrefabObject(GameObject modelObject)
        {
            modelObject.transform.parent = null;
            modelObject.SetActive(false);
            prefabPool[modelObject.name].AddLast(modelObject);
        }

        public GameObject GetPrefabObject(string name)
        {
            GameObject ret = null;

            if (!prototypePrefabs.ContainsKey(name))
            {
                ret = tryLoadStructure(name);

                if (ret == null)
                {
                    ret = tryLoadUnit(name);
                }

                if (ret == null)
                {
                    throw new ArgumentException("Object asked for is not a valid model");
                }

                prototypePrefabs.Add(name, ret);
            }


            if (prefabPool.ContainsKey(name) && prefabPool[name].Count > 0)
            {
                ret = prefabPool[name].Last.Value;
                ret.SetActive(true);
                prefabPool[name].RemoveLast();
            }
            else
            {
                ret = Object.Instantiate(prototypePrefabs[name]);
            }

            return ret;
        }

        private GameObject tryLoadStructure(string name)
        {
            return Resources.Load<GameObject>(STRUCTURE_PREFAB_DIR + name);
        }

        private GameObject tryLoadUnit(string name)
        {
            return Resources.Load<GameObject>(UNIT_PREFAB_DIR + name);
        }
    }
}
