﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public enum TreeSkinType { Summer, Winter }

    public class TreeSkinScript : MonoBehaviour
    {
        public Texture SummerTexture, WinterTexture;

        public void ApplyMaterial(TreeSkinType type)
        {
            switch (type)
            {
                case TreeSkinType.Summer:
                    GetComponent<Renderer>().material.mainTexture = SummerTexture;
                    break;
                case TreeSkinType.Winter:
                    GetComponent<Renderer>().material.mainTexture = WinterTexture;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type", type, null);
            }
        }
    }
}
