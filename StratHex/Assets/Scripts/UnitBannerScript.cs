﻿using Assets.Scripts.Game.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class UnitBannerScript : MonoBehaviour
    {
        private const string COUNTER_DIR = "Textures/UI/";

        private Unit unit;
        private Texture2D unitCounterTexture;

        public Image canvasBackground;
        public GameObject StrBar, OrgBar, MoraleBar, CounterImage;

        private GameObject cameraBase;

        // Use this for initialization
        void Start ()
        {
            cameraBase = GameObject.Find("HexMapCamera");
        }

        void Update()
        {
            Vector3 oldAngles = gameObject.transform.localEulerAngles;
            gameObject.transform.localEulerAngles = new Vector3(oldAngles.x, cameraBase.transform.localEulerAngles.y, oldAngles.z);
        }

        public void Initialize(Unit unit)
        {
            this.unit = unit;

            UpdateUI();
        }

        public void UpdateUI()
        {
            this.gameObject.transform.SetParent(unit.CurrentCell.transform, false);
            canvasBackground.color = Utils.GetColorFromUint(unit.Faction.Color);
            
            if(unitCounterTexture == null)
            {
                unitCounterTexture = Resources.Load<Texture2D>(COUNTER_DIR + unit.Stats.CounterName);
                CounterImage.GetComponent<Image>().sprite = Sprite.Create(unitCounterTexture, new Rect(0, 0, unitCounterTexture.width, unitCounterTexture.height), new Vector2());
            }

            StrBar.GetComponent<Slider>().value = (float)unit.Properties.Values[Unit.StatValues.Strength] / unit.Stats.Values[Unit.StatValues.Strength];
            OrgBar.GetComponent<Slider>().value = (float)unit.Properties.Values[Unit.StatValues.Organization] / unit.Stats.Values[Unit.StatValues.Organization];
            MoraleBar.GetComponent<Slider>().value = (float)unit.Properties.Values[Unit.StatValues.Morale] / unit.Stats.Values[Unit.StatValues.Morale];
        }

        public void DestroyUI()
        {
            GameObject.Destroy(this.gameObject);
        }
    }
}
