﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Assets.Scripts.Game;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.IO;
using Assets.Scripts.Game.Logic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Application = UnityEngine.Application;
using Button = UnityEngine.UI.Button;

namespace Assets.Scripts
{
    public class HexMapEditor : MonoBehaviour
    {
        enum MapEditorTool
        {
            None, Elevation, WaterLevel, Feature, Road, Terrain, River, Unit
        }

        enum BrushType
        {
            Hard, SoftRelative
        }

        public HexGrid hexGrid;
        public GameScript gameScript;
        public UIScript UiScript;

        int brushSize;
        private int activeFaction;
        private int activeValue = 0;
        private int activeSecondaryValue = 0;// used for feature levels
        private MapEditorTool activeTool = MapEditorTool.None;
        private BrushType activeBrush = BrushType.Hard;

        public GameObject TerrainButtonPrototype;
        public GameObject SideListButtonPrototype;

        private List<GameObject> coloredControls = new List<GameObject>();

        public Slider ElevationSlider, BrushSlider, WaterLevelSlider, FeatureSlider;
        public Text BrushLabel, FeaturesLabel;
        public Button NoRoadButton, AddRoadButton, NoRiverButton, AddRiverButton, AddUnitButton, RemoveUnitButton, BlueFactionButton, RedFactionButton, ElevationButton, WaterLevelButton, ForestButton, UrbanButton, RuralButton;
        protected List<Button> TerrainButtons;
        protected List<Button> SideListButtons = new List<Button>();

        public void RefreshEditorInterface()
        {
            foreach (GameObject coloredControl in coloredControls)
            {
                coloredControl.GetComponent<Image>().color = Color.white;
            }

            if (activeTool == MapEditorTool.Unit && activeValue != -1) {
                RefreshAndShowSideButtons();
                //EnableFactionButtons();
            } else {
                HideSideButtons();
                //DisableFactionButtons();
            }

            /*if (activeTool == MapEditorTool.Feature) {
                FeatureSlider.enabled = true;
            } else {
                FeatureSlider.enabled = false;
            }*/

            if (activeFaction == 0) {
                BlueFactionButton.GetComponent<Image>().color = Color.blue;
                RedFactionButton.GetComponent<Image>().color = Color.white;
            } else {
                BlueFactionButton.GetComponent<Image>().color = Color.white;
                RedFactionButton.GetComponent<Image>().color = Color.red;
            }

            switch (activeTool)
            {
                case MapEditorTool.None:
                    break;
                case MapEditorTool.Elevation:
                    ElevationButton.GetComponent<Image>().color = Color.green;
                    break;
                case MapEditorTool.WaterLevel:
                    WaterLevelButton.GetComponent<Image>().color = Color.green;
                    break;
                case MapEditorTool.Feature:
                    if (activeValue == 0) {
                        ForestButton.GetComponent<Image>().color = Color.green;
                    } else if (activeValue == 1) {
                        RuralButton.GetComponent<Image>().color = Color.green;
                    } else if (activeValue == 2) {
                        UrbanButton.GetComponent<Image>().color = Color.green;
                    }
                    break;
                case MapEditorTool.Road:
                    if(activeValue == -1)
                        NoRoadButton.GetComponent<Image>().color = Color.green;
                    else
                        AddRoadButton.GetComponent<Image>().color = Color.green;
                    break;
                case MapEditorTool.Terrain:
                    TerrainButtons[activeValue].GetComponent<Image>().color = Color.green;
                    break;
                case MapEditorTool.River:
                    if (activeValue == -1)
                        NoRiverButton.GetComponent<Image>().color = Color.green;
                    else
                        AddRiverButton.GetComponent<Image>().color = Color.green;
                    break;
                case MapEditorTool.Unit:
                    if(activeValue == -1)
                        RemoveUnitButton.GetComponent<Image>().color = Color.green;
                    else {
                        AddUnitButton.GetComponent<Image>().color = Color.green;

                        SideListButtons[activeValue].GetComponent<Image>().color = Color.green;
                    }
                    break;
            }
        }

        public void EnableFactionButtons()
        {
            BlueFactionButton.enabled = true;
            RedFactionButton.enabled = true;
        }

        public void DisableFactionButtons()
        {
            BlueFactionButton.enabled = false;
            RedFactionButton.enabled = false;
        }

        public void SetFaction(int idx)
        {
            activeFaction = idx;

            RefreshEditorInterface();
        }

        void Awake()
        {
            coloredControls.Add(NoRoadButton.gameObject);
            coloredControls.Add(AddRoadButton.gameObject);
            coloredControls.Add(ElevationButton.gameObject);
            coloredControls.Add(WaterLevelButton.gameObject);
            coloredControls.Add(AddRiverButton.gameObject);
            coloredControls.Add(NoRiverButton.gameObject);
            coloredControls.Add(AddUnitButton.gameObject);
            coloredControls.Add(RemoveUnitButton.gameObject);
            coloredControls.Add(RedFactionButton.gameObject);
            coloredControls.Add(BlueFactionButton.gameObject);
            coloredControls.Add(ForestButton.gameObject);
            coloredControls.Add(RuralButton.gameObject);
            coloredControls.Add(UrbanButton.gameObject);

            GenerateTerrainButtons();

            ElevationSlider.onValueChanged.AddListener(SetElevation);
            BrushSlider.onValueChanged.AddListener(SetBrushSize);
            WaterLevelSlider.onValueChanged.AddListener(SetWaterLevel);
            FeatureSlider.onValueChanged.AddListener(SetFeatureLevel);

            NoRoadButton.onClick.AddListener(() => {
                if (activeTool == MapEditorTool.Road && activeValue == -1)
                    activeTool = MapEditorTool.None;
                else {
                    activeValue = -1;
                    activeTool = MapEditorTool.Road;
                }

                RefreshEditorInterface();
            });
            AddRoadButton.onClick.AddListener(() => {
                if (activeTool == MapEditorTool.Road && activeValue >= 0)
                    activeTool = MapEditorTool.None;
                else {
                    activeValue = 2;
                    activeTool = MapEditorTool.Road;
                }

                RefreshEditorInterface();
            });

            NoRiverButton.onClick.AddListener(() => {
                if (activeTool == MapEditorTool.River && activeValue == -1)
                    activeTool = MapEditorTool.None;
                else {
                    activeValue = -1;
                    activeTool = MapEditorTool.River;
                }

                RefreshEditorInterface();
            });
            AddRiverButton.onClick.AddListener(() => {
                if(activeTool == MapEditorTool.River && activeValue >= 0)
                    activeTool = MapEditorTool.None;
                else {
                    activeValue = 1;
                    activeTool = MapEditorTool.River;
                }

                RefreshEditorInterface();
            });

            RemoveUnitButton.onClick.AddListener(() => {
                if (activeTool == MapEditorTool.Unit && activeValue == -1)
                    activeTool = MapEditorTool.None;
                else {
                    activeValue = -1;
                    activeTool = MapEditorTool.Unit;
                }

                RefreshEditorInterface();
            });
            AddUnitButton.onClick.AddListener(() => {
                if (activeTool == MapEditorTool.Unit && activeValue >= 0)
                    activeTool = MapEditorTool.None;
                else {
                    activeValue = 0;
                    activeTool = MapEditorTool.Unit;
                }

                RefreshEditorInterface();
            });

            ElevationButton.onClick.AddListener(() => {
                if (activeTool != MapEditorTool.Elevation)
                    SetElevation(ElevationSlider.value);
                else {
                    activeTool = MapEditorTool.None;
                }

                RefreshEditorInterface();
            });

            WaterLevelButton.onClick.AddListener(() => {
                if (activeTool != MapEditorTool.WaterLevel)
                    SetWaterLevel(WaterLevelSlider.value);
                else {
                    activeTool = MapEditorTool.None;
                }

                RefreshEditorInterface();
            });

            ForestButton.onClick.AddListener(() => {
                FeatureButtonClick(0);
            });

            RuralButton.onClick.AddListener(() => {
                FeatureButtonClick(1);
            });

            UrbanButton.onClick.AddListener(() => {
                FeatureButtonClick(2);
            });


            BlueFactionButton.onClick.AddListener(new UnityAction(() => { SetFaction(0); }));
            RedFactionButton.onClick.AddListener(new UnityAction(() => { SetFaction(1); }));

            activeFaction = 0;

            SetBrushSize(0);
            SetFeatureLevel(0);
            RefreshEditorInterface();
        }

        void Update()
        {
        }

        private void FeatureButtonClick(int featureIdx)
        {
            if (activeTool != MapEditorTool.Feature || activeValue != featureIdx) {
                activeTool = MapEditorTool.Feature;
                activeValue = featureIdx;

                SetFeatureLevel(FeatureSlider.value);
            } else {
                activeTool = MapEditorTool.None;
            }

            RefreshEditorInterface();
        }

        public void EditCells(HexCell center, bool isDrag, HexDirection dragDirection)
        {
            int centerX = center.coordinates.X;
            int centerZ = center.coordinates.Z;

            for (int r = 0, z = centerZ - brushSize; z <= centerZ; z++, r++)
            {
                for (int x = centerX - r; x <= centerX + brushSize; x++)
                {
                    HexCell cell = hexGrid.GetCell(new HexCoordinates(x, z));

                    if (cell != null)
                        EditCell(cell, isDrag, dragDirection);
                }
            }
            for (int r = 0, z = centerZ + brushSize; z > centerZ; z--, r++)
            {
                for (int x = centerX - brushSize; x <= centerX + r; x++)
                {
                    HexCell cell = hexGrid.GetCell(new HexCoordinates(x, z));

                    if(cell != null)
                        EditCell(cell, isDrag, dragDirection);
                }
            }
        }

        void EditCell(HexCell cell, bool isDrag, HexDirection dragDirection)
        {
            GameData gameData = GameLogic.Instance.gameData;

            if (activeTool == MapEditorTool.None)
                return;

            switch (activeTool)
            {
                case MapEditorTool.Elevation:
                    cell.Elevation = activeValue;
                    break;
                case MapEditorTool.WaterLevel:
                    cell.WaterLevel = activeValue;
                    break;
                case MapEditorTool.Feature:
                    SetCellFeature(cell, activeValue, activeSecondaryValue);
                    break;
                case MapEditorTool.Terrain:
                    cell.TerrainType = (TerrainType) activeValue;
                    break;
                case MapEditorTool.Unit:
                    if (activeValue == -1)
                    {
                        Unit unit = GameLogic.Instance.GetUnitAtCell(cell);
                        if (unit != null)
                            GameLogic.Instance.DestroyUnit(unit);
                    }

                    break;
            }

            if (activeTool != MapEditorTool.None)
                cell.chunk.Refresh();

            if (isDrag)
            {
                //interpret as operation on src cell rather than dst cell
                cell = cell.GetNeighbor(dragDirection.Opposite());

                switch (activeTool)
                {
                    case MapEditorTool.Road:
                        if (activeValue == -1)
                        {
                            cell.RemoveRoad(dragDirection);
                        }
                        else
                        {
                            cell.SetRoad(dragDirection, (RoadType)activeValue);
                        }
                        break;
                    case MapEditorTool.River:
                        if (activeValue == -1)
                        {
                            cell.RemoveRiver(dragDirection);
                        }
                        else
                        {
                            cell.AddRiver(dragDirection, true);
                        }
                        break;
                    case MapEditorTool.Unit:
                        if (activeValue >= 0)// remove unit handled without drag
                        {
                            if(GameLogic.Instance.GetUnitAtCell(cell) == null)
                            { 
                                Dictionary<string, Faction> factions = gameData.Factions;
                                GameLogic.Instance.CreateUnit(gameData.UnitTypes[gameData.UnitTypeNames[activeValue]], cell, dragDirection, factions.ElementAt(activeFaction).Value);

                                UiScript.UpdateUnitGraphics();
                            }
                        }
                        break;
                }
            }
        }

        public void SetCellFeature(HexCell cell, int featureIdx, int level)
        {
            if (featureIdx == 0) {
                cell.ForestLevel = level;
            } else if (featureIdx == 1) {
                cell.RuralLevel = level;
            } else if (featureIdx == 2) {
                cell.UrbanLevel = level;
            }

            ReduceFeatureSum(cell, featureIdx);
        }

        private void ReduceFeatureSum(HexCell cell, int keptFeatureIdx)
        {
            while (cell.ForestLevel + cell.RuralLevel + cell.UrbanLevel > 3) {
                for (int i = (keptFeatureIdx + 1) % 3; i != keptFeatureIdx; i = (i + 1) % 3) {
                    if (i == 0 && cell.ForestLevel > 0) {
                        cell.ForestLevel--;
                        break;
                    } else if (i == 1 && cell.RuralLevel > 0) {
                        cell.RuralLevel--;
                        break;
                    } else if (i == 2 && cell.UrbanLevel > 0) {
                        cell.UrbanLevel--;
                        break;
                    }
                }
            }
        }

        public void SetElevation(float elevation)
        {
            activeTool = MapEditorTool.Elevation;
            activeValue = (int) elevation;

            ElevationButton.GetComponentInChildren<Text>().text = "ELEV " + activeValue.ToString();
            RefreshEditorInterface();
        }

        public void SetWaterLevel(float level)
        {
            activeTool = MapEditorTool.WaterLevel;
            activeValue = (int)level;

            WaterLevelButton.GetComponentInChildren<Text>().text = "WATR " + activeValue.ToString();
            RefreshEditorInterface();
        }

        public void SetBrushSize(float size)
        {
            brushSize = (int) size;

            BrushLabel.text = brushSize != -1 ? (brushSize + 1).ToString() : "N/A";

            if ((int) BrushSlider.value != brushSize)
                BrushSlider.value = brushSize;
        }

        public void SetFeatureLevel(float level)
        {
            activeSecondaryValue = (int)level;

            FeaturesLabel.text = "Level " + activeSecondaryValue;
        }

        private void GenerateTerrainButtons()
        {
            if(TerrainButtons != null && TerrainButtons.Count > 0)
                throw new Exception("GenerateTerrainButtons called with terrain buttons already created");
            else
                TerrainButtons = new List<Button>();

            const float BUTTON_SPACING = 10;

            string[] terrainNames = Enum.GetNames(typeof (TerrainType));
            for (int i = 0; i < terrainNames.Length; i++)
            {
                GameObject button = GameObject.Instantiate(TerrainButtonPrototype, TerrainButtonPrototype.transform.parent);
                button.transform.position = button.transform.position + new Vector3(BUTTON_SPACING * i + button.GetComponent<RectTransform>().rect.width * i, 0, 0);

                button.GetComponentInChildren<Text>().text = terrainNames[i];
                int i1 = i;
                button.GetComponent<Button>().onClick.AddListener(() =>
                {
                    if (activeTool == MapEditorTool.Terrain && activeValue == i1)
                        activeTool = MapEditorTool.None;
                    else
                    {
                        activeValue = i1;
                        activeTool = MapEditorTool.Terrain;
                    }

                    RefreshEditorInterface();
                });

                coloredControls.Add(button);
                button.SetActive(true);
                TerrainButtons.Add(button.GetComponent<Button>());
            }
        }

        private Button getSideButton(int idx)
        {
            while (SideListButtons.Count <= idx)
            { 
                const float BUTTON_SPACING = 10;

                GameObject button = GameObject.Instantiate(SideListButtonPrototype, SideListButtonPrototype.transform.parent);
                button.transform.position = button.transform.position + new Vector3(0, -1f * (BUTTON_SPACING * idx + button.GetComponent<RectTransform>().rect.height * idx), 0);

                coloredControls.Add(button);
                SideListButtons.Add(button.GetComponent<Button>());
            }

            return SideListButtons[idx];
        }

        private void RefreshAndShowSideButtons()
        {
            GameData gameData = GameLogic.Instance.gameData;

            HideSideButtons();

            switch (activeTool)
            {
                case MapEditorTool.Unit:
                    for (int i = 0; i < GameLogic.Instance.gameData.UnitTypeNames.Count; i++)
                    {
                        Button button = getSideButton(i);
                        Unit.StatsCls unit = gameData.UnitTypes[gameData.UnitTypeNames[i]];

                        button.gameObject.GetComponentInChildren<Text>().text = unit.VisibleName;
                        button.gameObject.SetActive(true);

                        int i1 = i;
                        button.onClick.AddListener(() =>
                        {
                            activeValue = i1;

                            RefreshEditorInterface();
                        });
                    }
                    break;
                default:
                    Debug.LogError("Side buttons shown with tool that does not use them! " + activeTool.ToString());
                    break;
            }
        }

        private void HideSideButtons()
        {
            foreach (Button button in SideListButtons)
            {
                button.gameObject.SetActive(false);
            }
        }
    }
}
