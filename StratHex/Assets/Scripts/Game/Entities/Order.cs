﻿
namespace Assets.Scripts.Game.Entities
{
    public class Order
    {
        public OrderType Type { get; set; }

        public HexCell Target { get; set; }
        public Unit Source { get; set; }

        public static Order CreateMoveOrder(Unit source, HexCell target)
        {
            return new Order
            {
                Type = OrderType.Move,
                Source = source,
                Target = target
            };
        }
    }

    public enum OrderType
    {
        Move, Bombard
    }
}
