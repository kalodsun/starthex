﻿using System.Collections.Generic;

namespace Assets.Scripts.Game.Entities
{
    public class Faction
    {
        public Faction Parent { get; set; }
        public uint Color { get; set; }
        public Dictionary<Faction, Relation> Relations { get; set; }
        public string Name { get; set; }
        public string VisibleName { get; set; }

        public Faction()
        {
            Parent = null;
            Color = 0xFFFFFFFF;
            Relations = new Dictionary<Faction, Relation>();
            Name = "";
            VisibleName = "";
        }

        public Relation GetRelation(Faction other)
        {
            while (other != null)
            {
                Faction f = this;

                while (f != null)
                {
                    if (f.Relations.ContainsKey(other))
                        return f.Relations[other];

                    if (f.Equals(other))
                        return Relation.Ally;

                    f = f.Parent;
                }

                other = other.Parent;
            }

            return Relation.Enemy;//default
        }

        public enum Relation
        {
            Ally, Neutral, Enemy
        }
    }
}
