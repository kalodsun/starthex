﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Game.Entities;
using Assets.Scripts.Game.Logic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts {
    public class BattleInfoUI : MonoBehaviour
    {
        public Text BattleInfoTitle, BattleInfoText, RoundsInfoText;
        public GameObject RoundsInfoContent;
        public Button CloseButton;

        /*private TurnHistory.BattleHistory currentHistory;

        private static String getStatePastVerb(CombatState combatState)
        {
            switch (combatState) {
                case CombatState.Fighting:
                    return "won";
                case CombatState.Disengaging:
                    return "worn out";
                case CombatState.Disengaged:
                    return "retreated";
                case CombatState.Surrendered:
                    return "surrendered";
                default:
                    throw new ArgumentOutOfRangeException("combatState", combatState, null);
            }
        }

        private static String getStateContiniousVerb(CombatState combatState)
        {
            switch (combatState)
            {
                case CombatState.Fighting:
                    return "fighting";
                case CombatState.Disengaging:
                    return "disengaging";
                case CombatState.Disengaged:
                    return "disengaged";
                case CombatState.Surrendered:
                    return "surrendered";
                default:
                    throw new ArgumentOutOfRangeException("combatState", combatState, null);
            }
        }*/

        /*private String getSummaryString()
        {
            StringBuilder resultBuilder = new StringBuilder();

            float attackerCasulaties = 0;
            foreach (Unit attacker in currentHistory.Attackers) {
                attackerCasulaties += currentHistory.GetCasualtiesFloor(attacker);
            }

            resultBuilder.Append("Defender " + getStatePastVerb(currentHistory.Rounds.Last().DefenderState) + "\n");
            resultBuilder.Append("Casualties " + currentHistory.GetCasualtiesFloor(currentHistory.Defender) + " / " + attackerCasulaties + "\n");

            resultBuilder.Append("Org loss " + currentHistory.GetCasualtiesFloor(currentHistory.Defender, Unit.StatValues.Organization));
            resultBuilder.Append(" Morale loss " + currentHistory.GetCasualtiesFloor(currentHistory.Defender, Unit.StatValues.Morale) + "\n");

            resultBuilder.Append("Battle took " + currentHistory.Rounds.Count + " rounds so far");

            return resultBuilder.ToString();
        }*/

        /*public void DisplayBattleInfo(TurnHistory.BattleHistory battleHistory)
        {
            currentHistory = battleHistory;

            gameObject.SetActive(true);

            BattleInfoTitle.text = "Battle report: " + (battleHistory.IsDefenderVictorious() ? "Defender" : "Attacker") + " won";

            BattleInfoText.text = getSummaryString();

            StringBuilder roundReportBuilder = new StringBuilder();
            int roundInfoHeight = 10;
            for (int i = 0; i < currentHistory.Rounds.Count; i++) {
                TurnHistory.BattleRoundHistory round = currentHistory.Rounds[i];

                roundReportBuilder.Append("R" + i + " | ");
                roundReportBuilder.Append(round.GetValueFloor(Unit.StatValues.Strength, true) + "(" + currentHistory.GetCasualtiesForRoundFloor(Unit.StatValues.Strength, true, i) + ") / " 
                                          + round.GetValueFloor(Unit.StatValues.Strength, false) + "(" + currentHistory.GetCasualtiesForRoundFloor(Unit.StatValues.Strength, false, i) + ") | ");
                roundReportBuilder.Append(round.GetValueFloor(Unit.StatValues.Organization, true) + "(" + currentHistory.GetCasualtiesForRoundFloor(Unit.StatValues.Organization, true, i) + ") / "
                                          + round.GetValueFloor(Unit.StatValues.Organization, false) + "(" + currentHistory.GetCasualtiesForRoundFloor(Unit.StatValues.Organization, false, i) + ") | ");
                roundReportBuilder.Append(round.GetValueFloor(Unit.StatValues.Morale, true) + "(" + currentHistory.GetCasualtiesForRoundFloor(Unit.StatValues.Morale, true, i) + ") / "
                                          + round.GetValueFloor(Unit.StatValues.Morale, false) + "(" + currentHistory.GetCasualtiesForRoundFloor(Unit.StatValues.Morale, false, i) + ")\n");


                roundReportBuilder.Append("Attacker " + getStateContiniousVerb(round.AttackerState) + " " + round.AttackerSuccess.ToString("0.00") + 
                                          " Defender " + getStateContiniousVerb(round.DefenderState) + " " + round.DefenderSuccess.ToString("0.00") + "\n\n");

                roundInfoHeight += 65;
            }

            RoundsInfoContent.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, roundInfoHeight);
            RoundsInfoText.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, roundInfoHeight);
            RoundsInfoText.text = roundReportBuilder.ToString();
        }*/

        public void ClosePanel()
        {
            //currentHistory = null;
            gameObject.SetActive(false);
        }

        // Use this for initialization
        void Start () {
            CloseButton.onClick.AddListener(ClosePanel);
        }
	
        // Update is called once per frame
        void Update () {
		
        }
    }
}
